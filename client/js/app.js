var app = angular.module('myApp', ['ngCookies','autocomplete', 'ui.router','ngSanitize', 'ngFileUpload','ui.bootstrap']);
app.config(function ($stateProvider, $urlRouterProvider,$locationProvider) {

    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('home', {
            url: '/home',
            templateUrl: "views/home.html",
            controller: 'homeCtrl',
            authenticate: false

        })
        .state('audio_listing', {
            url: '/audio_listing',
            templateUrl: 'views/audio_listing.html',
            controller: 'audioListCtrl',
            authenticate: false
        })
        .state('audio_details', {
            url: '/audio_details',
            templateUrl: 'views/audio_details.html',
            controller: 'incidentAudioCtrl',
            authenticate: false
        })
        .state('video_details', {
            url: '/video_details',
            templateUrl: 'views/video_details.html',
            authenticate: false
        })
        .state('categories', {
            url: '/categories',
            templateUrl: 'views/categories.html',
            controller: 'catCtrl',
            authenticate: false
        })
        .state('category_details', {
            url: '/category/:categoryAllies',
            templateUrl: 'views/category_details.html',
            controller: 'catDetailsCtrl',
            authenticate: false
        })

        .state('adminpage', {
            url: '/adminpage',
            templateUrl: "views/adminpage.html",
            controller: 'adminCtrl',
            authenticate: true
        })
        .state('audio_video', {
            url: '/audio_video',
            templateUrl: 'views/audio_video.html',
            authenticate: false
            // controller:'audiovideoCtrl'

        })
        .state('logIn', {
            url: '/login',
            templateUrl: 'views/logIn.html',
            controller: 'signinCtrl',
            authenticate: false
        })
        .state('uploadform',{
            url: '/uploadform',
            templateUrl: 'views/uploadform.html',
            controller:'uploadFormCtrl',
            authenticate: true
        })
        .state('searchedItem',{
            url: '/searchedItem?referer',
            templateUrl: 'views/searchedItem.html',
            controller:'searchedCtrl',
            authenticate: false
        })
        .state('userpage',{
            url:'/user',
            templateUrl: 'views/userPage.html',
            authenticate: true
        })
        .state('about',{
            url:'/about-us',
            templateUrl: 'views/about.html',
            authenticate: false
        })
        .state('contact',{
            url:'/contact',
            templateUrl: 'views/contact.html',
            controller:'contactusctrl',
            authenticate: false
        })
        .state('usersList',{
            url:'/users_list',
            templateUrl: 'views/userlist.html',
            controller:'userslistCtrl',
            authenticate: true
        })
        .state('accountSetting',{
            url:'/accountsetting',
            templateUrl: 'views/account-settings.html',
            controller:'accountsetCtrl',
            authenticate: true
        })
        .state('viewIncident',{
            url:'/viewincident',
            templateUrl:'views/view_incidents.html',
            controller : 'viewincidentCtrl'

        })
        .state('description', {
            url: '/:obj',
            templateUrl: 'views/description.html',
            controller: 'descriptionctrl',
            authenticate: false
        })
    //$locationProvider.html5Mode(true);
});

app.value('$anchorScroll', angular.noop);
app.run(function($cookies,$rootScope,$location,$state,$stateParams,AuthService,adapter,$window) {

    function setIntialUser(){
        $rootScope.currentUser = {
            isLoggedIn:false,
            user:{},
            otherDetails:{
                image:''
            }
        };
    }
    setIntialUser();
    $rootScope.$on('$viewContentLoaded', function(){
        var state = $state.$current;
        if (state.scrollTo == undefined) $window.scrollTo(0, 0);
        else {
            var to = 0;
            if (state.scrollTo.id != undefined)
                to = $(state.scrollTo.id).offset().top;
            if ($($window).scrollTop() == to)
                return;
            if (state.scrollTo.animated)
                $(document.body).animate({scrollTop:to});
            else
                $window.scrollTo(0, to);
        }
    });
    if(AuthService.loggedIn()){
        $rootScope['currentUser']['isLoggedIn'] = AuthService.loggedIn();
        getUser('text',function(){
        });
    }
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        $rootScope.title = toState.title;
        if(fromState.name == 'viewincident'){
            $rootScope.$broadcast('stopAudio', '');
        }

        if (toState.authenticate && !AuthService.loggedIn()){
            event.preventDefault();
            $state.go('home');
        }
    });
    function getUser(val,next){
        var userAccessToken=$cookies.getObject('accessTokenDetail');

        var request = {
            method: 'get',
            command: 'usersRegistartions/'+userAccessToken.data.userId,
            headers:{
                access_token:userAccessToken.id
            }
        };
        adapter.call(request)
            .then(function (data) {
                if(data){
                    $rootScope.setCurrentUser(data,function(){
                        next()
                    });


                }
            })
            .catch(function (err) {

            })
    }
    $rootScope.setCurrentUser = function (user,next) {
        $rootScope.currentUser['isLoggedIn']=true;
        $rootScope.currentUser['user'] = user;
        localStorage.setItem("cUser",JSON.stringify($rootScope.currentUser))
        next()
    };
    $rootScope.$on('auth-login-success',function(event,args){
        getUser(args.userId,function(){

            if($rootScope.currentUser.user && $rootScope.currentUser.user.role=='admin'){
                $state.go("adminpage")}
            else{
                $state.go('userpage')
            }
            if($rootScope.currentUser && $rootScope.currentUser.isLoggedIn){
                if($rootScope.currentUser.user){
                    var type=$rootScope.currentUser.user.usertype;

                }
            }
        });
    })

    $rootScope.isNullOrEmpty = function(str) {
        if (str == null || str == undefined || str == '') {
            return true;
        }
        else {
            return false;
        }
    }

    $rootScope.logOut=function() {
        $window.location.reload();
        $cookies.remove('accessTokenDetail');
        $cookies.remove('emailId');
        localStorage.removeItem('cUser');
        setIntialUser();
        $state.go('home');



    }
});


app.directive('googleAdsense', function () {
    return {
        restrict: 'AE',
        replace: true,

        template: '<ins class="adsbygoogle"  style="display:inline-block;width:100%;height:90px;margin:0 auto; text-align:center;" data-ad-client="ca-pub-3921091883166944" data-ad-slot="8035351792"></ins>',

        controller: function () {
            console.log("VicigoApp: Google Adsense is being added!");
            (adsbygoogle = window.adsbygoogle || []).push({});
        }
    }

});
app.constant('AUTH_EVENTS', {
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized'
})
app.service('Session', function () {
    this.create = function (sessionId, userId, userRole) {
        this.id = sessionId;
        this.userId = userId;
        this.userRole = userRole;
    };
    this.destroy = function () {
        this.id = null;
        this.userId = null;
        this.userRole = null;
    };
});
app.filter('reverse', function() {
    return function(items) {
        return items.slice().reverse();
    };
});
app.filter("emptyToEnd", function () {
    return function (array, key) {
        if (!angular.isArray(array)) return;
        var present = array.filter(function (item) {
            console.log(">>>>>>>>>>item[fk;fvkf;lvdkey]",key,"fdfdfdf")
            return item[0][key];
        });
        var empty = array.filter(function (item) {
            console.log(">>>>>>>>>>item[key]",item[key],"fdfdfdf")
            return !item[key]
        });
        return present.concat(empty);
    };
});
app.factory('AuthService', function ($http, Session,$cookies) {
    var authService = {};
    authService.login = function (credentials) {
        return $http
            .post('/login', credentials)
            .then(function (res) {
                Session.create(res.data.id, res.data.user.id,
                    res.data.user.role);
                return res.data.user;
            });
    };
    authService.loggedIn = function () {
        var userAccessToken=$cookies.getObject('accessTokenDetail');
        if(!userAccessToken){
            return false;8035351792
        }
        else{
            return true;
        }

    };
    return authService;
})
app.filter('trusted', ['$sce', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
}]);
app.factory('ajaxInterceptor', ['$q', '$location', "$rootScope", function ($q, $location, $rootScope) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            config.headers['ajax'] = 1;
            return config;
        }
    };
}])
app.filter('comparedate',function(){
    function comparedate(postDate) {
        debugger
        if (postDate) {
            var date = new Date(postDate);
            if (isNaN(date)) {

            }
            else {
                var today = new Date();
                today.setHours(0, 0, 0, 0);
                date.setHours(0, 0, 0, 0);
                var dateTense = date > today ? convertDate(date) : date < today ? convertDate(date) : "Today at";
                return dateTense;
            }
        }
    }

    function convertDate(inputFormat) {
        function pad(s) {
            return (s < 10) ? '0' + s : s;
        }

        var d = new Date(inputFormat);
        return [pad(d.getDate()), pad(d.getMonth() + 1), d.getFullYear()].join('/');
    }
    return function(input){
        debugger
        var postedDate = new Date(input);
        var dateTenses = comparedate(postedDate);
        return dateTenses ;
    }

})
app.filter('discription', function ($filter) {
    function formatAMPM(date) {
        var InUtc =  $filter('utc')(date);
        var formateDate = $filter('date')(InUtc, 'MM-dd-yyyy h:mm a').split(" ");
        var strTime = formateDate[1] +" "+ formateDate[2];
        return strTime;
    }
    return function (input) {

        var name ="";

        var date = formatAMPM(new Date(input.postedOn))
        if (input.city != null && input.city != " ") {
            name = input.city;

        }

        if (input.location != null && input.location != " ") {
            name += '-' + input.location

        }
        if (input.event_type != null && input.event_type != " ") {
            if (name != "") {

                name += '-';
            }
            name += input.event_type;

        }
        if (input.description != null && input.description != " ") {
            if (name != "") {

                name += '-';
            }
            name += input.description;

        }
        if(name == ""){
            name = input.name;
        }
        return " "+ date + " - " + name ;
    }

});
//app.directive('truncateString', function($timeout) {
//
//    return {
//        restrict: 'A',
//
//        link: function(scope, elem, attr, ctrl, transclude) {
//            var lengths = 0;
//            $timeout(function(){
//                var len = elem[0].innerText.replace(/(\r\n|\n|\r)/gm,"").length ;
//                if(len > 100){
//                    var a = '<div class="seeMore" onclick="showMore(this)">read more</div>'
//                    // elem.append(a);
//                    $(a).insertAfter("#"+elem[0].id)
//                }
//
//
//            },100)
//
//            attr.$observe('id', function(id) {
//                $('.seeMore').remove();
//                var node = $('#'+attr.id);
//                node.className = 'discDiv';
//
//                var len = node[0].innerText.replace(/(\r\n|\n|\r)/gm,"").length ;
//                if(len > 100){
//                    var a = '<div class="seeMore" onclick="showMore(this)">read more</div>'
//                    $(a).insertAfter(node)
//                }
//            })
//
//        }
//    }
//
//});
app.factory("adapter", ["$http", "$q", "$rootScope", function ($http, $q, $rootScope) {
    var Adapter = function () {
        var self = this;
        self.call = function (request) {
            if(!request.headers){
                header={
                    "Content-Type": "application/json"
                }
            }

            if (!request.method ) {
                return false;
            }
            if(request.headers){
                header=request.headers;
                header['Content-Type']='application/json';
            }
            var deferred = $q.defer();
            switch (request.method.toLowerCase()) {
                case 'get' :
                    if (!request.query) {
                        request.query = {};
                    }
                    if (request.query.filter) {
                        request.query.filter = JSON.stringify(request.query.filter);
                    }
                    $http.get(BASE_URL+request.command, {params: request.query,headers:header}).then(function (response) {
                        deferred.resolve(response.data);
                    }, function (error) {
                        deferred.reject(error.data);
                    })
                    return deferred.promise;

                    break;
                case  'post' :
                    $http.post(BASE_URL+request.command, request.data,{headers:header}).then(function (response) {
                        deferred.resolve(response.data);
                    }, function (error) {
                        deferred.reject(error.data);
                    })
                    return deferred.promise;

                    break;
                case 'put':
                    $http.put(BASE_URL+request.command, request.data,{headers:header}).then(function (response) {
                        deferred.resolve(response.data);
                    }, function (error) {
                        deferred.reject(error.data);
                    })
                    return deferred.promise;
                    break;
                case  'delete' :
                    $http.delete(BASE_URL+request.command, request.data,{headers:header}).then(function (response) {
                        deferred.resolve(response.data);
                    }, function (error) {
                        deferred.reject(error.data);
                    })
                    return deferred.promise;

                    break;
                default :

            }
        };

    }
    return new Adapter();
}])
app.filter('utc', function () {

    return function (val) {
        var date = new Date(val);
        var minutes = date.getUTCMinutes();
        minutes = minutes-minutes%5;
        return new Date(date.getUTCFullYear(),
            date.getUTCMonth(),
            date.getUTCDate(),
            date.getUTCHours(),
            minutes,
            date.getUTCSeconds());
    };

});
app.directive('fileUploader', function () {
    return {
        restrict: 'E',
        transclude: true,
        template: '<div><input type="file" multiple /><button class="btn-primary"ng-click="upload()">Upload</button></div>'
        + '<ul><li ng-repeat="file in files">{{file.name}} - {{file.type}}</li></ul>',
        controller: function ($scope, $fileUpload) {
            $scope.notReady = true;
            $scope.upload = function () {
                $fileUpload.upload($scope.files);
            };
        },
        link: function ($scope, $element) {
            var fileInput = $element.find('input[type="file"]');
            fileInput.bind('change', function (e) {
                $scope.notReady = e.target.files.length == 0;
                $scope.files = [];
                for (i in e.target.files) {
                    //Only push if the type is object for some stupid-ass reason browsers like to include functions and other junk
                    if (typeof e.target.files[i] == 'object') $scope.files.push(e.target.files[i]);
                }
            });
        }
    }
});
app.directive('pwCheck', [function () {
    return {
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            var firstPassword = '#' + attrs.pwCheck;
            elem.add(firstPassword).on('keyup', function () {
                scope.$apply(function () {
                    var v = elem.val() === $(firstPassword).val();
                    ctrl.$setValidity('pwmatch', v);
                });
            });
        }
    }
}]);
app.directive('loading',function($http){
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };
            scope.$watch(scope.isLoading, function (value) {
                if (value) {

                    element.removeClass('ng-hide');
                } else {

                    element.addClass('ng-hide');
                }
            });
        }
    };
});
app.directive('truncateString', function($timeout) {
    function readMore(myStr,ele){
        var maxLength = 300;
        if($.trim(myStr).length > maxLength){
            var newStr = myStr.substring(0, maxLength).replace(/\r?\n/g, '<br />');
            var removedStr = myStr.substring(maxLength, $.trim(myStr).length).replace(/\r?\n/g, '<br />').replace(/["']/g, "");
            var s = "<span class='disData'>"+"<small>"+newStr +"</small>"+"</span>"
            $("#"+ele.id).empty().html(s);
            $("#"+ele.id).append(' <a href="javascript:void(0);" removestr=" '+removedStr+' " text="removedStr" onclick="readmore(this)" class="seeMore">read more...</a>');

        }
    }
    return {
        restrict: 'A',

        link: function(scope, elem, attr, ctrl, transclude) {
            var lengths = 0;
            $timeout(function(){

                var myStr = elem[0].innerText;
                readMore(myStr,elem[0]);
            },100)

            attr.$observe('id', function(id) {
                $('.seeMore').remove();
                var node = $('#'+attr.id);
                node.className = 'discDiv';
                var myStr = elem[0].innerText;
                readMore(myStr,elem[0]);
            })
        }
    }


});
app.directive('onErrorSrc', function() {
    return {
        link: function(scope, element, attrs) {
            element.bind('error', function() {
                if (attrs.src != attrs.onErrorSrc) {
                    attrs.$set('src', attrs.onErrorSrc);
                }
            });
        }
    }
});

app.filter('htmlToPlaintext', function() {
    return function(text) {
        return  text ? String(text).replace(/<[^>]+>/gm, '') : '';
    };
});





