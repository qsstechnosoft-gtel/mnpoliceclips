'use strict';
app.service('catService', function ($http,$q) {


  this.getCat = function (id) {
      var deferred = $q.defer();
      var filter={
          "filter":{"where":{"active":1}}
      }
      $http.get(BASE_URL+'Categories',{params:filter})
          .success(function (response) {
              deferred.resolve(response);

          })
          .error(function (error) {
              deferred.reject(error);
          });
      return deferred.promise;
  }
});
app.service('recentClips',function($http,$q){
  this.recent = function () {
    var deferred = $q.defer();
    var filter={
      "filter":{"where":{"active":1},"order":"postedOn DESC","limit":"50"}
    }
    $http.get(BASE_URL+'Transmissions',{params:filter})
        .success(function (response) {
          deferred.resolve(response);

        })
        .error(function (error) {
          deferred.reject(error);
        });
    return deferred.promise;
  }
});
app.service('requestedClip',function($http,$q){
  this.recent = function (id) {
  
    var deferred = $q.defer();
    var filters = {
      "filter": {
        "where": {"seoUrl":'/'+id}
      }
    }
    $http.get(BASE_URL+'Transmissions',{params:filters})
        .success(function (response) {
        
          deferred.resolve(response);

        })
        .error(function (error) {
          deferred.reject(error);
        });
    return deferred.promise;
  }
});
app.service('reqCategory_data',function($http){
  this.reqData = function (id,skip) {
    if(id){
     
      var filters = {
        "filter": {
          "where": {
            "categoryId": id
          },
          "order": "postedOn DESC",
          "limit":"50"
        }
      }
    }
    else{
     
      var filters = {
        "filter": {
          "order": "postedOn DESC",
          "limit":"50"
        }
      }
    }



    //filter=JSON.stringify(filter);
    var promise = $http.get(BASE_URL + 'Transmissions', {params: filters})
        .then(function (response) {

          return response.data;
        });
    return promise;
  }
});
// app.service('siginService', function ($http) {
//   this.signinFun=function(){
//     $http.get(BASE_URL+'Users/login')
//   }
// });
app.factory("sessionService",function(){
  return{
    set:function(key,value){
      sessionStorage.setItem(key,value)
    },
    get:function(key){
      sessionStorage.getItem(key)
    },
    destroy:function(key){
      sessionStorage.removeItem(key)
    }
  }
});
app.service('requestClip_count',function ($http) {
  this.reqData = function (id) {
    var promise= $http.get(BASE_URL + 'Transmissions/count?where={"categoryId":"' + id + '"}')
        .then(function(response){
          
          return response.count;
        });
    return promise;
  }
})

/*app.factory("loginService",function($http,$location,$rootScope,sessionService,$cookies,$state,AUTH_EVENTS) {
 return {
 login:function(data,scope){
 var adminData=JSON.parse(data);
 $http.post(BASE_URL + "usersRegistartions/login",data)
 .then(function (response,data) {
 console.log(">>>>>>>>>>>>>>.response is>>>>",response)
 var expireDate = new Date();
 expireDate.setDate(expireDate.getDate() + 5);
 $cookies.putObject('accessTokenDetail', response, {expires: expireDate});
 $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, response);


 }, function (err) {
 //Priyanka work
 document.getElementById("wrongemailndpass").innerHTML = "Wrong Credential";
 //console.log("enter correct email and password");
 })
 },
 loggedIn:function(){
 if(sessionService.get('user')) return true;
 }
 }
 });*/
app.service("loginService",function($http,$q,$location,$rootScope,sessionService,$cookies,$state,AUTH_EVENTS) {
  this.login=function(data,scope){
    var adminData=JSON.parse(data);
    var res={};
    res['data']={};
    var deferred = $q.defer();
    $http.post(BASE_URL + "usersRegistartions/login?include=user",data).success(function(response){
      var expireDate = new Date();
      res['data']=response;
      expireDate.setDate(expireDate.getDate() + 5);

      $cookies.putObject('accessTokenDetail', res, {expires: expireDate});
      $rootScope.$broadcast(AUTH_EVENTS.loginSuccess, res);
      deferred.resolve(response);
    }).error(function(response,status){
      deferred.resolve(status);
    });
    return deferred.promise;
  },
      this.loggedIn=function(){
        if(sessionService.get('user')) return true;
      }
});
app.service("adminAudioVideo",function($http,$q){
  this.askData=function(adminId) {
   
    var promise=$http.get(BASE_URL+'Transmissions?filter={"where":{"postedBy":"'+adminId+'"}}')
        .then(function (response) {
         
          return response.data;
        });
    return promise;
  }
});
app.service('searchItem',function($http){
  this.reqData = function (item) {
    var filters= {
      "filter": {

        "where":
        {
          "or":[
            {"name":{"regexp":"/"+item+"*/"}},
            {"description":{"regexp":"/"+item+"*/"}},
            {"seoUrl":{"regexp":"/"+item+"*/"} } ]
        }
      }
    }

    //filter=JSON.stringify(filter);
  
    var promise = $http.get(BASE_URL+'Transmissions',{params:filters})
        .then(function (response) {

          return response.data;
        });
    return promise;
  }
});
app.service("categoryName",function($http,$q){
  this.name=function(catId) {
    var promise=$http.get(BASE_URL+'Categories/'+catId)
        .then(function (response) {
         
          return response.data;
        });
    return promise;
  }
});
app.service("categoryId",function($http){
  this.getId=function(name) {
    var filters = {
      "filter": {
        "where": {"allies": name}
      }
    }

    var promise = $http.get(BASE_URL + 'Categories', {params: filters})
        .then(function (response) {
         
          return response.data;
        });
    return promise;
  }
});
app.service("deletedata",function($http){
  this.delAudio=function(id) {
    var promise = $http.delete(BASE_URL + 'Transmissions/'+id)
        .then(function (response) {
          return response.data;
          swal({
            title: "Success",
            text: 'Deleted file successfully',
            type: "success",
            timer: 2000,
            animation: "slide-from-top",
            showOkButton: false
          });
        });
    return promise;
  }
});
app.service('requestupdateClip',function($http,$q){
  this.recent = function (id) {
   
    var deferred = $q.defer();
    $http.get(BASE_URL+'Transmissions/'+id)
        .success(function (response) {
        
          deferred.resolve(response);

        })
        .error(function (error) {
          deferred.reject(error);
        });
    return deferred.promise;
  }
});
app.factory('validation', function () {
  var date_regex = /-(0[1-9]|1[0-2])[-\/](0[1-9]|1\d|2\d|3[01])[-\/](19|20)\d{2}$/
  var validate = {};
  validate.testDate = function (testDate) {
    var matches = /^(\d{1,2})[-\/](\d{1,2})[-\/](\d{4})$/.exec(testDate);
    if (matches == null) {
      return false;
    }
    else{
      return true;
    }

  }
  validate.isStGreaterEd = function(startDate,endDate){
    if(Date.parse(startDate) > Date.parse(endDate)){
      return false;

    }else{
      return true
    }

  }
  return validate;

});

// http://localhost:3000/api/Transmissions?filter[where][postedBy]=
// http://localhost:4000/api/Transmissions?filter={"where":{"postedBy":"skv@g.com"}}
// BASE_URL+'Transmissions?filter={"where":{"postedBy":"'+adminId+'"}}'
// http://52.52.155.247:4000/api/Transmissions?filter=%7B%22where%22%3A%7B%22id%22%3A%224%22%7D%7D
