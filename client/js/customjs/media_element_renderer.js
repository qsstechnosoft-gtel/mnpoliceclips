(function(Weebly, $){
	var mediaElements = [];

	var findWidth = function ($container) {
		var width = $container.width();
		width = (width && width < 400) ? (width < 235) ? 235 : width : 400;
		return width;
	};

	var resizeAudioElements = function() {
		$.each(mediaElements, function(){
			var $parent = $(this.container).parents('.wsite-html5audio');

			var width = findWidth($parent.parent());

			this.setPlayerSize(width, this.height);
			this.setControlsSize();
		});
	};

	var init = function($context) {
		var $audio = $('.wsite-html5audio audio', $context),
			autoplayTriggered = false;

		mediaElements = [];

		$audio.each(function() {
			var $this = $(this),
				$parent = $this.parents('.wsite-html5audio'),
				trackName = $this.attr('data-track'),
				artistName = $this.attr('data-artist'),
				autostart = $this.attr('data-autostart'),
				width;

			$this.attr('preload','none');

			autostart = (autostart === 'yes') ? true : false;
			if (autostart && !autoplayTriggered) {
				//$this.attr('autoplay', 'autoplay');
				autoplayTriggered = true;
			} else {
				autostart = false;
			}

			width = findWidth($parent.parent());

			var options = {
				// width of audio player
				audioWidth: width,
				enableKeyboard: false,
				success: function(media, dom, player) {
					if (Weebly.mobile_navigation && Weebly.mobile_navigation.resizeScreen) {
						Weebly.mobile_navigation.resizeScreen();
					}
					blurControls(media, player);
				}
			};

			var src = $this.attr('src');
			if (typeof src == 'undefined' || src === null || src == '' || src.charAt( src.length - 1 ) == '/') {
				options.type = 'audio/mp3';
			}
			if (autostart) {
				options.success = function(media, dom, player) {
					if (player && player.play) {
						if (media.pluginType === 'native') {
							player.load();
						}
						blurControls(media, player);
						player.play();
					}
				};
			}
			var player = new mejs.MediaElementPlayer($this, options);
			mediaElements.push(player);

			var $container = $parent.find('.mejs-container');
			if ($container.length > 0) {
				var trackExists = (trackName && trackName.length > 0),
					artistExists = (artistName && artistName.length > 0);

				var artistHtml = '<div class="wsite-mejs-track">';
				if (trackExists && artistExists) {
					artistHtml += '<span class="wsite-mejs-title">' + trackName + '</span>'
							+ '<span class="wsite-mejs-track-sep"> - </span>'
							+ '<span class="wsite-mejs-artist">' + artistName + '</span>'
						+ '</div>';
				} else if (trackExists) {
					artistHtml += '<span class="wsite-mejs-title">' + trackName + '</span>'
						+ '</div>';
				} else if (artistExists) {
					artistHtml += '<span class="wsite-mejs-artist">' + artistName + '</span>'
						+ '</div>';
				} else {
					artistHtml = '';
				}
				$container.append(artistHtml);
			}
		});

		// Stops keyboard input from pausing/playing the player after it's been started.
		// Duplicated in AudioElement.js
		function blurControls(media, player) {
			var playerButtons = $('#' + player.id).find('button');
			media.addEventListener('play', function() {
				playerButtons.blur();
			}, false);

			media.addEventListener('pause', function() {
				playerButtons.blur();
			}, false);
		}

	};

	$(function() {
		if (Weebly.EDITOR) {
			editorApp.vent.on('themeRendered', resizeAudioElements);
			return;
		}

		init();
		// resize elements if scrollbar is showing
		resizeAudioElements();
		$(window).load(resizeAudioElements);
	});

	Weebly.MediaElement = {};
	Weebly.MediaElement.init = init;

}(Weebly, Weebly.jQuery));
