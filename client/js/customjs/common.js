


$(document).ready(function() {
	'use strict';
	//custom scroll for dropdown menu
    $('#dropMenu').slimScroll({
        height: '250px',
		color: '#1b4080',
   		railVisible: true,
    	alwaysVisible: true,
    	railOpacity: 1,
		//wheelStep: 10,
    });
	
	//Sign In
	$('.signIn').on('click', function(){
		$('.signinWrap').toggleClass('open');
		$('.signupWrap').removeClass('open');
		$('.frgtPwd').removeClass('open');
	});
	
	$('.signUp').on('click', function(){
		$('.signupWrap').toggleClass('open'); 
		$('.signinWrap').removeClass('open');
		$('.frgtPwd').removeClass('open');
	});
	
	$('.signinOverlay').on('click', function(){
		$('.signupWrap').removeClass('open'); 
		$('.signinWrap').removeClass('open');
		$('.frgtPwd').removeClass('open');
	});
	
	$('.frgtBtn').on('click', function(){
		$('.signupWrap').removeClass('open'); 
		$('.signinWrap').removeClass('open');
		$('.frgtPwd').toggleClass('open');
	});
	$('	header .signinWrap h3, header .signupWrap h3, header .frgtPwd h3').on('click', function(){
		$('.signupWrap').removeClass('open'); 
		$('.signinWrap').removeClass('open');
		$('.frgtPwd').removeClass('open');
	});
	
});


	(function(jQuery) {
		try {
			if (jQuery) {
				jQuery('div.blog-social div.fb-like').attr('class', 'blog-social-item blog-fb-like');
				var $commentFrame = jQuery('#commentArea iframe');
				if ($commentFrame.length > 0) {
					var frameHeight = jQuery($commentFrame[0].contentWindow.document).height() + 50;
					$commentFrame.css('min-height', frameHeight + 'px');
				}
				if (jQuery('.product-button').length > 0){
					jQuery(document).ready(function(){
						jQuery('.product-button').parent().each(function(index, product){
							if(jQuery(product).attr('target') == 'paypal'){
								if (!jQuery(product).find('> [name="bn"]').length){
									jQuery('<input>').attr({
										type: 'hidden',
										name: 'bn',
										value: 'DragAndDropBuil_SP_EC'
									}).appendTo(product);
								}
							}
						});
					});
				}
			}
			else {
				// Prototype
				$$('div.blog-social div.fb-like').each(function(div) {
					div.className = 'blog-social-item blog-fb-like';
				});
				$$('#commentArea iframe').each(function(iframe) {
					iframe.style.minHeight = '410px';
				});
			}
		}
		catch(ex) {}
	})(window._W && _W.jQuery);
	
	$('.SlectBox').SumoSelect();
	$('audio').mediaelementplayer(/* Options */);
	

$('video').mediaelementplayer({
	success: function(media, node, player) {
		$('#' + node.id + '-mode').html('mode: ' + media.pluginType);
	}
});



	
	
