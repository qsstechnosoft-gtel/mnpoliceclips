/**
 * Created by azmat on 1/12/16.
 */
function isNullOrEmpty(str) {
    if (str == null || str == undefined || str == '') {
        return true;
    }
    else {
        return false;
    }
}
function cloneData(a) {

    return JSON.parse(JSON.stringify(a));
}
app.controller("homeCtrl", function (catService, $state, recentClips, $scope, adapter, $timeout) {

    $scope.showme = true;
    catService.getCat().then(function (allcategory) {
        $scope.cat = allcategory;
    });
    $scope.slectedCategory = 'Historical';
    $scope.defaultCategorytoFind = 15;
    $scope.query = {};
    $scope.queryBy = '$';
    $scope.list = [];
    $scope.showbutton = true;
    var limit = 50;
    var skip = 0;
    var user = JSON.parse(localStorage.getItem("cUser"));
    if (isNullOrEmpty(user)) {
        $scope.isLoggIn = false;

    } else {
        $scope.isLoggIn = user.isLoggedIn;
    }
    getSearchData('allData', 'id', skip, limit, function (data) {
        $scope.list = data;
        if (isNullOrEmpty(data)) {
            $scope.showbutton = false;
        }
    });
    /* getSearchData('catData', $scope.defaultCategorytoFind, skip, limit, function (data) {
     $scope.slectedCategory = 'Historical';
     $scope.list = data;
     if (isNullOrEmpty(data)) {
     $scope.showbutton = false;
     }
     });*/
    $scope.clipmessage= "";
    $scope.selected = null;
    function getSearchData(val, id, skip1, limit, next) {
        var request = {};
        if (val == 'catData') {
            request = {
                method: 'get',
                command: 'Transmissions',
                query: {
                    filter: {
                        "where": {

                            "categoryId": id
                        },
                        order: "postedOn DESC,id DESC",
                        limit: limit,
                        skip: skip1
                    }
                }

            };
        }
        if (val == 'allData') {
            if( $scope.isLoggIn ){
                request = {
                    method: 'get',
                    command: 'Transmissions',
                    query: {
                        filter: {
                            order: "postedOn DESC,id DESC",
                            limit: limit,
                            skip: skip1
                        }
                    }

                };
            }
            else{
                request = {
                    method: 'get',
                    command: 'Transmissions',
                    query: {
                        filter: {
                            where: {  "or": [

                                {
                                    "ismember": false
                                },

                                {
                                    "ismember": null
                                }
                            ]},
                            order: "postedOn DESC,id DESC",
                            limit: limit,
                            skip: skip1
                        }
                    }

                };
            }

        }

        adapter.call(request)
            .then(function (data) {

                if (data && data.length) {

                    next(data)
                }
                else {
                    loadMore =false;
                    next(data)
                }

            }, function (error) {
                next()
            })

    }
    $('#scrollingDiv').scroll(function(){

        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            if ($scope.showbutton && $scope.showbutton == true) {

                if (isNullOrEmpty($scope.catId)) {

                    getSearchData('allData', 'id', $scope.list.length, limit, function (data) {
                        $scope.list = $scope.list.concat(data);
                        skip = $scope.list.length;
                        if (data && !data.length) {

                            $scope.showbutton = false;
                        }

                    })
                }
                else {
                    getSearchData('catData', $scope.catId, $scope.list.length, limit, function (data) {
                        $scope.list = $scope.list.concat(data);
                        skip = $scope.list.length;
                        if (data && !data.length) {
                            $scope.showbutton = false;
                        }

                    })
                }
            }
        }
    })










    $scope.sendSeoUrlParam = function (seourl) {


        if (seourl) {
            var urls = seourl.split('/');

            if (urls && urls.length) {
                var mainSeoUrl = urls.pop();

                $state.go('description', { obj: mainSeoUrl });
            }
        }
    }


    $scope.countUp = function() {
        $scope.clipmessage= "No Clips found";
    }

    $timeout($scope.countUp, 1000);


    $scope.findall = function () {
        $scope.showme = true
        var limit = 50;
        var skip = 0;
        $scope.selected = null;
        getSearchData('allData', 'id', skip, limit, function (data) {
            $scope.list = data;
            if (isNullOrEmpty(data)) {
                $scope.showbutton = false;
            }
        })

    }

    $scope.find = function (id, index, name) {
        $scope.slectedCategory = name;
        $scope.selected = index;
        $scope.catId = id;
        var skip = 0
        var limit = 50
        getSearchData('catData', id, skip, limit, function (data) {
            $scope.list = data;
            if (isNullOrEmpty(data)) {
                $scope.showbutton = false;
            }
        })
    }
    ;


    $('.navWrap ul').click(function () {
        $('.navWrap h3').toggleClass('open');
        $('.navWrap ul').toggleClass('open');
    });
    $(".navWrap ul").on("tap", function () {

        $('.navWrap h3').toggleClass('open');
        $('.navWrap ul').toggleClass('open');
    });
    $scope.sendSeoUrlParam = function (seourl) {

        if (seourl) {
            var urls = seourl.split('/');

            if (urls && urls.length) {
                var mainSeoUrl = urls.pop()

                $state.go('description', { obj: mainSeoUrl });
            }
        }
    }
});
app.controller("catCtrl", function (catService, $scope) {
    catService.getCat().then(function (d) {
        $scope.cat = d;
    })

});
//app.controller("audioListCtrl", function (recentClips, $scope) {
//
//    recentClips.recent().then(function (d) {
//        $scope.rFeed = d;
//    })
//    $scope.limit = 5;
//    $scope.loadMore = function () {
//        if ($scope.limit <= 45) {
//            $scope.limit = $scope.limit + 5;
//            $scope.btnHide = false;
//        }
//        else {
//            $scope.btnHide = true;
//        }
//    }
//});
app.controller("incidentAudioCtrl", function (incidentAudio, $scope) {
    incidentAudio.iAudio().then(function (d) {
        $scope.voice = d;
    })
});
app.controller("signupCtrl", function ($scope, $rootScope, $state, $http, $filter, adapter) {
    $scope.fields = {};
    $scope.formData = {};
    $scope.errors = {};
    $scope.signup = {};
    $scope.signUpForm = {};
    //$scope.registerform={};

    var emailRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    $scope.submitted = false;
    if (!isNullOrEmpty($scope.$parent.user)) {

        $scope.fields.role = $scope.$parent.user;
    }
    $scope.submitForm = function () {
        $scope.errors = {};
        $scope.submitted = true;

        if ($scope.fields.email && !emailRegex.test($scope.fields.email)) $scope.errors['email'] = "Invalid email";
        if ($scope.signUpForm.$invalid) {

            return;
        }
        var matchPassword = function () {
            if ($scope.fields.password && ($scope.fields.password != $scope.fields.confirm_password) && $scope.fields.confirm_password) {

                $scope.errors['confirm_password'] = " password not match";
            }
        };
        matchPassword();
        if (Object.keys($scope.errors).length) {
            return;
        }
        $('.signupWrap').removeClass('open');
        var data = {
            id: 0,
            email: $scope.fields.email,
            firstName: $scope.fields.fname,
            lastName: $scope.fields.lname,
            phoneNo: $scope.fields.pnumber,
            password: $scope.fields.password,
            createdOn: $filter('date')(new Date(), 'yyyy-MM-dd HH:mm'),
            role: $scope.fields.role,
            active: 1
        };
        $http.post(BASE_URL + 'usersRegistartions', data).success(function (data) {
            swal({
                title: "Success",
                text: $scope.fields.role + " registered successfully",
                type: "success",
                timer: 2000,
                animation: "slide-from-top",
                showOkButton: false
            });
            $('#addUser').modal('hide');
            $scope.fields = {};
            $scope.$parent.clearselectuser();
            $scope.$parent.updateEdit();


        }).error(function (error) {
            swal({
                title: "Fail",
                text: $scope.fields.role + " already registered",
                type: "error",
                timer: 2000,
                animation: "slide-from-top",
                showOkButton: false
            });
            $scope.fields = {};
            $scope.$parent.clearselectuser();
        })
    }
    $scope.changePassword = function () {
        $scope.errors = {};
        if (!$scope.formData.oldpassword) $scope.errors['oldpassword'] = "Please enter current password";
        if (!$scope.formData.password) $scope.errors['password'] = "Please enter password";
        if (!$scope.formData.confirm_password) $scope.errors['confirm_password'] = "Please enter confirm password";
        var matchPassword = function () {
            if (($scope.formData.password != '' && $scope.formData.password != $scope.formData.confirm_password) || isNullOrEmpty($scope.formData.confirm_password)) {
                $scope.errors['confirm_password'] = "Confirm password do not match";
            }
        };
        matchPassword();
        if (Object.keys($scope.errors).length) {
            return false;
        }
        var request = {
            method: 'post',
            command: 'usersRegistartions/password-change',
            data: $scope.formData
        };
        request['data']['id'] = $rootScope.currentUser.user.id;
        adapter.call(request)
            .then(function (data) {
                $scope.formData = {}

                $('#changePass').modal('hide');
                swal({
                    title: "Success",
                    text: "Password Changed Successfully",
                    type: "success",
                    timer: 2000,
                    animation: "slide-from-top",
                    showOkButton: false
                });

            })
            .catch(function (err) {
                if (err) {
                    swal({
                        title: "Error",
                        text: "Wrong Password",
                        type: "error",
                        timer: 2000,
                        animation: "slide-from-top",
                        showOkButton: false
                    });
                }

            })
    }


});
app.controller("catDetailsCtrl", function ($scope, $state, validation, $stateParams, adapter, $filter, $location, reqCategory_data, categoryId) {
    var name = $stateParams.categoryAllies;
    var currentUser = JSON.parse(localStorage.getItem("cUser"));
    $scope.isLoggIn = false;
    if (!isNullOrEmpty(currentUser)) {
        $scope.isLoggIn = true;

    }
    categoryId.getId(name).then(function (data) {


        $scope.categoryId = data[0].id;

        $scope.categoryname = data[0].name;
        $scope.query = {}
        $scope.queryBy = '$';
        $scope.list = [];
        $scope.showbutton = false;
        $scope.loadmore = false;
        $scope.msg = "";
        $scope.noclips = false;
        $scope.strequier = false;
        $scope.edrequier = false;
        $scope.searchval = false;
        $scope.date = {};
        $scope.date.startdate = "";
        $scope.date.enddate = "";
        var limit = 50;
        var skip = 0;

        getSearchData('getall', $scope.categoryId, skip, limit, 'date', 'searchitem', function (data) {
            if (!isNullOrEmpty(data)) {
                $scope.showbutton = true;
                $scope.loadmore = true;
                $scope.list = data;

            }
            else{
                $scope.noclips = true;
            }
        })
        function getSearchData(val, id, skip1, limit, date, searchitem, next) {


            if (!isNullOrEmpty(date.startdate) && !isNullOrEmpty(date.enddate)) {
                var startdate = new Date(date.startdate);
                var enddate = new Date(date.enddate);
                startdate = $filter('date')(new Date(startdate), 'yyyy-MM-dd');
                enddate = $filter('date')(new Date(enddate), 'yyyy-MM-dd 23:59:59');

            }

            var request = {};
            if (val == 'getall') {
                request = {
                    method: 'get',
                    command: 'Transmissions',
                    query: {
                        filter: {
                            "where": {

                                "categoryId": id
                            },
                            order: "postedOn DESC",
                            limit: limit,
                            skip: skip1
                        }
                    }
                };
            }
            if (val == 'search') {
                request = {
                    method: 'get',
                    command: 'Transmissions',
                    query: {}
                };
                if (!isNullOrEmpty(startdate) && !isNullOrEmpty(enddate) && !isNullOrEmpty(searchitem)) {
                    request.query.filter = {
                        "where": {
                            "and": [
                                {
                                    "postedOn": {
                                        "between": [startdate, enddate]
                                    }
                                },
                                {
                                    "or": [
                                        {
                                            "name": {
                                                "regexp": "/" + searchitem + "*/"
                                            }
                                        },
                                        {
                                            "description": {
                                                "regexp": "/" + searchitem + "*/"
                                            }
                                        },
                                        {
                                            "seoUrl": {
                                                "regexp": "/" + searchitem + "*/"
                                            }
                                        }
                                    ]
                                },
                                {
                                    "categoryId": id
                                }
                            ]
                        },
                        order: "postedOn DESC",
                        limit: limit,
                        skip: skip1
                    };
                }
                else if (!isNullOrEmpty(searchitem) && isNullOrEmpty(startdate) && isNullOrEmpty(enddate)) {
                    request.query.filter = {
                        "where": {
                            "and": [
                                {
                                    "categoryId": id
                                },
                                {
                                    "or": [
                                        {
                                            "name": {
                                                "regexp": "/" + searchitem + "*/"
                                            }
                                        },
                                        {
                                            "description": {
                                                "regexp": "/" + searchitem + "*/"
                                            }
                                        },
                                        {
                                            "seoUrl": {
                                                "regexp": "/" + searchitem + "*/"
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        order: "postedOn DESC",
                        limit: limit,
                        skip: skip1
                    };
                }
                else if (isNullOrEmpty(searchitem) && !isNullOrEmpty(startdate) && !isNullOrEmpty(enddate)) {
                    request.query.filter = {
                        "where": {
                            "postedOn": {
                                "between": [startdate, enddate]
                            },
                            "categoryId": id
                        },
                        order: "postedOn DESC",
                        limit: limit,
                        skip: skip1
                    };
                }
                else {
                    request.query.filter = {
                        "where": {
                            "categoryId": id
                        },
                        order: "postedOn DESC",
                        limit: limit,
                        skip: skip1
                    };
                }
            }


            adapter.call(request)
                .then(function (data) {
                    if (data && data.length) {

                        next(data)
                    }
                    else {
                        next(data)
                    }

                }, function (error) {
                    next(data)
                })

        }


        $('#scrollingDiv2').bind('scroll', function () {
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                if ($scope.showbutton == true && $scope.loadmore == true) {
                    getSearchData('search', $scope.categoryId, $scope.list.length, limit, $scope.date, $scope.searchItem, function (data) {
                        $scope.list = $scope.list.concat(data);
                        skip = $scope.list.length
                        if (isNullOrEmpty($scope.list)) {
                            $scope.loadmore = false;
                            $scope.noclips = true;
                        }
                        else{
                            $scope.noclips = false;
                        }
                    })
                }
                else {
                    $scope.message = "No More Clip Found"
                }
            }
        })






        $scope.sendSeoUrlParam = function (seourl) {

            if (seourl) {
                var urls = seourl.split('/');

                if (urls && urls.length) {
                    var mainSeoUrl = urls.pop();

                    $state.go('description', { obj: mainSeoUrl });
                }
            }
        }

        $scope.onchangeDate = function (dateT, value) {
            if (!isNullOrEmpty(value)) {
                $scope.strequier = false;
                $scope.edrequier = false;
                $scope.searchval = false;
                $scope.msg = "";
            }
        }
        $scope.find_clip = function (id, searchItem, date) {
            $scope.noclips = false
            if ((isNullOrEmpty($scope.date.startdate) && isNullOrEmpty($scope.date.enddate)) && isNullOrEmpty(searchItem)) {


                $scope.strequier = true;
                $scope.edrequier = true;
                $scope.searchval = true;
                $scope.msg = "Please enter values";
                return;
            }
            else {
                $scope.searchval = false;
                $scope.strequier = false;
                $scope.edrequier = false;
                $scope.msg = "";


            }
            if (isNullOrEmpty($scope.date.startdate) && !isNullOrEmpty($scope.date.enddate)) {

                $scope.strequier = true;
                $scope.msg = "For search by date start date and end date are required";

                return;
            }
            if (isNullOrEmpty($scope.date.enddate) && !isNullOrEmpty($scope.date.startdate)) {

                $scope.edrequier = true;
                $scope.msg = "For search by date start date and end date are required";
                return;
            }
            if (!isNullOrEmpty($scope.date.enddate) && !isNullOrEmpty($scope.date.startdate)) {
                if (!validation.testDate($scope.date.enddate) || !validation.testDate($scope.date.startdate)) {
                    $scope.msg = "Please enter valid date";
                    return

                }

                if (!validation.isStGreaterEd($scope.date.startdate, $scope.date.enddate)) {
                    $scope.date.startdate = "";
                    $scope.date.enddate = "";
                    $scope.msg = "End date should be greater than start date";
                    return;
                }

            }

            skip = 0;

            getSearchData('search', $scope.categoryId, skip, limit, $scope.date, searchItem, function (data) {
                $scope.list = data;
                if(isNullOrEmpty(data)){
                    $scope.noclips = true ;
                }


            })
        }
        $scope.reset = function () {
            var skip = 0;
            $scope.msg = false;
            $scope.strequier = false;
            $scope.edrequier = false;
            $scope.searchval = false;
            $scope.noclips = false ;
            getSearchData('getall', $scope.categoryId, skip, limit, 'date', 'searchItem', function (data) {

                $scope.list = data;
                if(isNullOrEmpty(data)){
                    $scope.noclips = true ;
                }
                $scope.date = "";
                $scope.searchItem = "";
            })

        }
        //$scope.searchClip=function()
        //{   skip=0
        //    if(!isNullOrEmpty($scope.date)){
        //        getAdminData('search',id,limit,skip,$scope.date,$scope.searchItem,function(data){
        //            $scope.adminData = data;
        //        })
        //    }
        //    else{
        //        getAdminData('search',id,limit,skip,'date',$scope.searchItem,function(data){
        //            $scope.adminData = data;
        //        })
        //    }
        //
        //}
    });


});

app.controller("signinCtrl", function ($scope, $http, loginService, sessionService, $cookies, $state, $rootScope, adapter) {
    $scope.wrongCredential = false;
    $scope.showforgottemplate = false;
    $scope.showotpText = false;
    $scope.errors = {};
    $scope.sendingOtp = false;
    $scope.fields = {};
    $scope.signInForm = {}
    var emailRegex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

    function isNullOrEmpty(str) {
        if (str == null || str == undefined || str == '') {
            return true;
        }
        else {
            return false;
        }
    }

    $scope.signinForm = function () {
        $scope.errors = {};
        if (!$scope.fields.email) $scope.errors['email'] = "Please enter email";
        if ($scope.fields.email && !emailRegex.test($scope.fields.email)) $scope.errors['email'] = "Invalid Email";
        if (!$scope.fields.password) $scope.errors['password'] = "Please enter password";

        if (Object.keys($scope.errors).length) {
            return false;
        }
        // $('.signinWrap').removeClass('open');
        var Parameter = JSON.stringify($scope.fields);
        var promise = loginService.login(Parameter);
        promise.then(function (status) {

            if (status == 401) {
                $scope.wrongCredential = true;
            }
            else {
                $scope.wrongCredential = false;
                $('.signinWrap').removeClass('open');

            }

        });
        $scope.fields = {}

    }
    $scope.checkForgotEmail = function () {
        $scope.errors = {};


        if (!emailRegex.test($scope.fields.email)) {
            $scope.errors['email'] = 'Invalid Email ID';
            return;
        }
        $scope.sendingOtp = true;
        var request = {
            method: 'post',
            command: 'usersRegistartions/forgot-password',
            data: $scope.fields
        };
        adapter.call(request)
            .then(function (data, status) {
                $scope.fields = {};
                $('.frgtPwd').removeClass('open');
                swal({
                    title: "Success",
                    text: "Password Reset Successfully",
                    type: "success",
                    timer: 2000,
                    animation: "slide-from-top",
                    showOkButton: false
                });
            })
            .catch(function (err) {
                $scope.errors = {}
                $scope.sendingOtp = false;
                if (err && err.error) {
                    $scope.errors['message'] = err.error.message;
                }
            })


    }
    $scope.verifyOtp = function (otp) {

        if ($scope.fields && $scope.fields['otp'] && $scope.fields['email']) {
            var request = {
                method: 'post',
                command: 'usersRegistartions/verifyotp',
                data: $scope.fields
            };
            adapter.call(request)
                .then(function (data) {
                    if (data && data.otp) {
                        $scope.showotpText = false;
                        $scope.showforgottemplate = true;
                    }
                    else {

                    }
                })
                .catch(function (err) {
                    if (err && err.error) {
                        $scope.errors['message'] = err.error.message;
                    }
                })
        }

    }
    $scope.changePassword = function () {
        $scope.errors = {}
        var matchPassword = function () {
            if (($scope.fields.password != '' && $scope.fields.password != $scope.fields.confirm_password) || isNullOrEmpty($scope.fields.confirm_password)) {
                $scope.errors['confirm_password'] = "Confirm password do not match";
                return false;
            }
            else {
                return true;
            }
        };
        if (!matchPassword()) {
            return;
        }

        var requestdata = {
            "email": $scope.fields['email'],
            "password": $scope.fields['password']
        }
        var request = {
            method: 'post',
            command: 'usersRegistartions/changePassword',
            data: $scope.fields
        };
        adapter.call(request)
            .then(function (userData) {
                $scope.fields = {};
                $('.frgtPwd').removeClass('open');
                swal({
                    title: "Success",
                    text: "Password Reset Successfully",
                    type: "success",
                    timer: 2000,
                    animation: "slide-from-top",
                    showOkButton: false
                });
            })
            .catch(function (err) {

            })

    };

});
app.controller("adminCtrl", function ($state, $scope, catService, adminAudioVideo, adapter, $rootScope, requestedClip, deletedata, $timeout, $compile, $cookies, $http, $location, $window) {
    $scope.noRecords = false;
    $scope.index = null;
    $scope.numArray = [10, 20, 30, 40, 50, 60, 70, 80, 90, 100];
    $scope.setLimit = 50;
    $scope.showbutton = true;
    $scope.categorysearch = false;
    $scope.itemsearch = false;
    $scope.adminData = [];
    var skip = 0;
    function getAdminData(val, id, limit, skip, searchItem, next) {
        if (val == 'test1') {
            swal({
                title: "Success",
                text: 'Deleted file successfully',
                type: "success",
                timer: 2000,
                animation: "slide-from-top",
                showOkButton: false
            });
        }

        $scope.copyToClipboard = function (index, data) {

            if (data && data.seoUrl) {
                var mainUrl = data.seoUrl.split('/')
                var url = mainUrl.pop()

                var finalString = $location.protocol() + '://' + $location.host()
                /* if($location.port()){
                 finalString = finalString+':'+$location.port()
                 }*/
                finalString = finalString + '/clips/' + url
                var aux = document.createElement("input");
                aux.setAttribute("value", finalString);
                document.body.appendChild(aux);
                aux.select();
                $scope.index = index;
                document.execCommand("copy");
                document.body.removeChild(aux);
                $timeout(function () {

                    $scope.index = null;
                }, 3000)
            }

        }

        var request = {}

        if (val == 'catUpload') {
            request = {
                method: 'get',
                command: 'Transmissions',
                query: {
                    filter: {
                        "where": { "categoryId": id }, "order": "id DESC",
                        "limit": limit,
                        skip: skip
                    }
                }
            };
        }
        if (val == 'test' || val == 'test1') {
            request = {
                method: 'get',
                command: 'Transmissions',
                query: {
                    filter: {
                        "order": "id DESC",
                        "limit": limit,
                        skip: skip
                    }
                }
            };
        }
        if (val == 'search') {

            if (!isNullOrEmpty(id)&& !isNullOrEmpty(searchItem)) {
                request = {
                    method: 'get',
                    command: 'Transmissions',
                    query: {
                        filter: {
                            "where": {
                                "and": [
                                    {
                                        "categoryId": id
                                    },
                                    {
                                        "or": [
                                            {
                                                "name": {
                                                    "regexp": "/" + searchItem + "*/"
                                                }
                                            },
                                            {
                                                "description": {
                                                    "regexp": "/" + searchItem + "*/"
                                                }
                                            },
                                            {
                                                "seoUrl": {
                                                    "regexp": "/" + searchItem + "*/"
                                                }
                                            }
                                        ]
                                    }
                                ]
                            },
                            order: "id DESC",
                            limit: limit,
                            skip: skip
                        }
                    }


                };
            }
            else if (!isNullOrEmpty(id)&& isNullOrEmpty(searchItem)) {
                request = {
                    method: 'get',
                    command: 'Transmissions',
                    query: {
                        filter: {
                            "where": {

                                "categoryId": id

                            },
                            order: "id DESC",
                            limit: limit,
                            skip: skip
                        }
                    }


                };
            }
            else if (isNullOrEmpty(id)&& !isNullOrEmpty(searchItem)) {
                request = {
                    method: 'get',
                    command: 'Transmissions',
                    query: {
                        filter: {
                            "where": {
                                "or": [
                                    { "name": { "regexp": "/" + searchItem + "*/" } },
                                    { "description": { "regexp": "/" + searchItem + "*/" } },
                                    { "seoUrl": { "regexp": "/" + searchItem + "*/" } }]
                            },
                            order: "id DESC",
                            limit: limit,
                            skip: skip
                        }
                    }


                };
            }
            else {
                request = {
                    method: 'get',
                    command: 'Transmissions',
                    query: {
                        filter: {
                            "order": "id DESC",
                            "limit": limit,
                            skip: skip
                        }
                    }


                }
            };


        }
        adapter.call(request)
            .then(function (data) {
                if (data && data.length) {
                    next(data)
                }
                else {
                    next(data)
                }

            }, function (error) {
                next()
            })
    }

    var currentUser = JSON.parse(localStorage.getItem("cUser"));

    if (currentUser.isLoggedIn == true) {


        if (currentUser.user.role == 'admin') {

            $('#navbar').removeClass('in');
            $('.navbar-toggle.ResposiveNavBtn').addClass('collapsed');
            $('.signinWrap').removeClass('open');
            catService.getCat().then(function (allcategory) {
                $scope.categories = allcategory;
                getAdminData('test', 'text', $scope.setLimit, skip, "searchitem", function (data) {
                    $scope.adminData = data;
                    if (data && !data.length) {
                        $scope.showbutton = false;
                    }
                })
            });
        }
        else {

            $state.go('home');
        }
    }
    $scope.updateEdit = function () {
        skip = 0;
        catService.getCat().then(function (allcategory) {
            $scope.categories = allcategory;
            getAdminData('test', 'text', $scope.setLimit, skip, 'searchItem', function (data) {
                $scope.adminData = data;
            })
        });
    }
    $scope.categoryUploads = function (catId) {

        skip = 0
        $scope.currentcatid = catId;
        if (!isNullOrEmpty(catId) && isNullOrEmpty($scope.searchItem)) {
            $scope.categorysearch = true;
            $scope.itemsearch = false;
            getAdminData('catUpload', catId, $scope.setLimit, skip, 'searchItem', function (data) {

                $scope.adminData = data;
            });
        }
        else if (!isNullOrEmpty(catId) && !isNullOrEmpty($scope.searchItem)) {
            $scope.itemsearch = true;
            $scope.categorysearch = false;
            getAdminData('search', catId, $scope.setLimit, skip, $scope.searchItem, function (data) {

                $scope.adminData = data;
            });
        }
        else {
            $scope.allCat();
        }


    }
    $scope.sendSeoUrlParam = function (seourl) {

        if (seourl) {
            var urls = seourl.split('/');

            if (urls && urls.length) {
                var mainSeoUrl = urls.pop()

                $state.go('description', { obj: mainSeoUrl });
            }
        }
    }
    $scope.allCat = function () {
        skip = 0;
        $scope.searchItem = "";
        $scope.categorysearch = false;
        $scope.itemsearch = false;
        getAdminData('test', 'text', $scope.setLimit, skip, 'searchItem', function (data) {
            $scope.adminData = data;
        })


    };
    $scope.reset = function () {
        $scope.noRecords = false;
        $scope.searchItem = "";
        $scope.selectedItem = "";
        $scope.categorysearch = false;
        $scope.itemsearch = false;
        $scope.setLimit = 50;
        skip = 0;
        getAdminData('test', 'text', $scope.setLimit, skip, 'searchItem', function (data) {
            $scope.adminData = data;
        })
    }
    $scope.deletedata = function (item, index) {
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Yes, I am sure!',
                cancelButtonText: "No, cancel it!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    $http.delete(BASE_URL + 'Transmissions/' + item).then(function (data) {
                        $scope.adminData.splice(index, 1);
                        swal({
                            title: "Success",
                            text: 'Deleted file successfully',
                            type: "success",
                            timer: 2000,
                            animation: "slide-from-top",
                            showOkButton: false
                        });
                        // getAdminData('test1', 'text', $scope.setLimit, skip, 'searchItem', function (data) {
                        //     $scope.adminData = [];
                        //     $scope.adminData = data;
                        // })
                    }, function (error) {
                    })

                } else {
                    swal("Cancelled", "Your file is safe :)", "error");
                }
            });
    }
    $scope.updateclip = function (id) {
        $("#updateclip").modal();
        requestedClip.recent(id).then(function (data) {
            $scope.name = data[0].name;
        })
    }
    $scope.filterScopeString = "Filter";
    $scope.viewSearBar = function()
    {
        $scope.filterScopeString = $scope.filterScopeString == 'Filter' ? 'Close' : 'Filter';
        $("#filterbox").slideToggle();
        //  $scope.filterScopeString =
    }

    angular.element($window).bind('scroll', function () {
        var scrollTop = $(window).scrollTop();
        var docHeight = $(document).height();
        var winHeight = $(window).height();
        var scrollPercent = (scrollTop) / (docHeight - winHeight);
        var scrollPercentRounded = Math.round(scrollPercent*100);
        if (scrollPercentRounded >= 95 ) {
            $('#back-to-top').fadeIn();
            if ($scope.showbutton && $scope.showbutton == true && "/adminpage" == $location.path()) {
                if ($scope.categorysearch == true) {
                    getAdminData('catUpload', $scope.currentcatid, $scope.setLimit, $scope.adminData.length, 'searchItem', function (data) {
                        $scope.adminData = $scope.adminData.concat(data);
                        skip = $scope.adminData.length
                        if (data && !data.length) {
                            $scope.showbutton = false;
                        }

                    })
                }
                else if ($scope.itemsearch == true) {
                    getAdminData('search', $scope.currentcatid, $scope.setLimit, $scope.adminData.length, $scope.searchItem, function (data) {

                        $scope.adminData = $scope.adminData.concat(data);
                        skip = $scope.adminData.length
                        if (data && !data.length) {
                            $scope.showbutton = false;
                        }

                    })
                }
                else if ($scope.categorysearch == false && $scope.itemsearch == false) {
                    getAdminData('test', 'text', $scope.setLimit, $scope.adminData.length, 'searchItem', function (data) {

                        $scope.adminData = $scope.adminData.concat(data);
                        skip = $scope.adminData.length
                        if (data && !data.length) {
                            $scope.showbutton = false;
                        }

                    })
                }
            }
        }
        else{
            $('#back-to-top').fadeOut();
        }
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $(window).scrollTop(0);
            // $('#back-to-top').fadeOut();
        });

        $('#back-to-top').tooltip('show');
    });
    $scope.searchClip = function () {
        $scope.noRecords = false;
        $scope.currentcatid = $scope.selectedItem;
        $scope.categorysearch = false;
        skip = 0
        $scope.categorysearch = false;
        $scope.itemsearch = true;
        getAdminData('search', $scope.selectedItem, $scope.setLimit, skip, $scope.searchItem, function (data) {

            $scope.adminData = data;
            if (isNullOrEmpty(data)) {
                $scope.showbutton = false;
                $scope.noRecords = true ;
            }
        })


    }

});

app.controller("signoutCtrl", function ($rootScope, $scope, $timeout, $cookies, $route, $state, $window) {

    $scope.signoutFun = function () {


        localStorage.setItem("cUser",'');
        $state.go("home");
        $rootScope.signedIn = false;

        $scope.email = "";
        $scope.password = "";

    }
});
app.controller('descriptionctrl', function ($state, $sce, $scope, $location,  requestedClip, categoryName) {
    var id = $state.params.obj;

    if(id == '')
        $location.path('/home');
    requestedClip.recent(id).then(function (data) {
        if (data && data.length) {
            $scope.data = data[0];



            var catId = data[0].categoryId;
            categoryName.name(catId).then(function (d) {

                $scope.categoryallies = d.allies;
            })
        }
        else
        {
            // $location.path('/home');
        }
    })
    var currentUser = JSON.parse(localStorage.getItem("cUser"));
    $scope.isLoggIn = false;
    if (!isNullOrEmpty(currentUser)) {
        $scope.isLoggIn = true;
        if (currentUser.isLoggedIn == true && currentUser.user.role == 'admin') {
            $scope.user = currentUser.user.role;
        }
    }

    $scope.sendSeoUrlParam = function (seourl) {

        if (seourl) {
            var urls = seourl.split('/');

            if (urls && urls.length) {
                var mainSeoUrl = urls.pop().substr(1);

                $state.go('description', { obj: mainSeoUrl });
            }
        }
    }
});
app.controller("searchCtrl", function ($scope, $state, recentClips, $location) {

    $(".search_field").keyup(function (event) {
        if (event.keyCode == 13) {
            $scope.search();
        }
    });

    $scope.search = function () {
        var abc = $scope.searchItem
        var searchdata = "searchedItem?search=" + abc

        // $location.path(searchdata);
        //$scope.$apply()

        $(".search_field").blur();

        $scope.showip = false;
        $state.go('searchedItem', { referer: $scope.searchItem })

        $scope.searchItem = "";
    }
})
app.controller("searchedCtrl", function ($scope, validation, $state, $stateParams, $location, $filter, searchItem, adapter) {
    $scope.strequier = false;
    $scope.edrequier = false;
    $scope.loading = false;
    $scope.showbutton = false
    $scope.date = {};
    $scope.date.startdate = "";
    $scope.date.enddate = "";
    $scope.msg = "";
    $scope.query = {}
    $scope.queryBy = '$';
    $scope.list = [];
    var limit = 50;
    var skip = 0;
    $scope.onchangeDate = function (dateT, value) {
        $scope.msg = "";

        if (dateT == 'sd') {
            if (!isNullOrEmpty(value)) {
                $scope.strequier = false;
            }
        }
        if (dateT == 'ed') {
            if (!isNullOrEmpty(value)) {
                $scope.edrequier = false;
            }
        }

    }
    function getSearchData(val, item, skip1, limit, date, next) {

        if (val == 'search') {
            var request = {
                method: 'get',
                command: 'Transmissions',
                query: {
                    filter: {
                        "where": {
                            "or": [
                                { "name": { "regexp": "/" + item + "*/" } },
                                { "description": { "regexp": "/" + item + "*/" } },
                                { "seoUrl": { "regexp": "/" + item + "*/" } }]
                        },
                        order: "postedOn DESC",
                        limit: limit,
                        skip: skip1
                    }
                }


            };
        }
        else if (val == 'find_date') {

            var startdate = $filter('date')(new Date(date.startdate), 'yyyy-MM-dd');
            var enddate = $filter('date')(new Date(date.enddate), 'yyyy-MM-dd 23:59:59');

            var request = {
                method: 'get',
                command: 'Transmissions',
                query: {
                    filter: {
                        "where": {
                            "and": [
                                {
                                    "postedOn": {
                                        "between": [startdate, enddate]
                                    }
                                },
                                {
                                    "or": [
                                        {
                                            "name": {
                                                "regexp": "/" + item + "*/"
                                            }
                                        },
                                        {
                                            "description": {
                                                "regexp": "/" + item + "*/"
                                            }
                                        },
                                        {
                                            "seoUrl": {
                                                "regexp": "/" + item + "*/"
                                            }
                                        }
                                    ]
                                }
                            ]
                        },
                        order: "postedOn DESC",
                        limit: limit,
                        skip: skip1
                    }


                }

            };
        }
        adapter.call(request)
            .then(function (data) {
                if (data && data.length) {

                    $scope.dateclick = true;
                    $scope.showbutton = true;
                    next(data)
                }
                else {
                    next(data)
                }

            }, function (error) {
                next()
            })


    }

    if (Object.keys($location.search()).length) {
        var searchkeyword = $location.search();

        $scope.searchvalue = searchkeyword.referer;

        getSearchData('search', $scope.searchvalue, skip, limit, 'date', function (data) {

            $scope.list = $scope.list.concat(data);
            skip = $scope.list.length

            if (!isNullOrEmpty(data)) {
                $scope.showbutton = true;
            }

        })

        $('#scrollingDiv3').bind('scroll', function () {
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                if ($scope.showbutton == true) {

                    if (!isNullOrEmpty($scope.date.startdate) && !isNullOrEmpty($scope.date.enddate)) {

                        var date = $scope.date;
                        getSearchData('find_date', $scope.searchvalue, $scope.list.length, limit, date, function (data) {
                            $scope.list = $scope.list.concat(data);
                            skip = $scope.list.length
                            if (data && !data.length) {
                                $scope.loading = false;
                            }

                        })
                    }
                    else {

                        getSearchData('search', $scope.searchvalue, $scope.list.length, limit, 'date', function (data) {

                            $scope.list = $scope.list.concat(data);
                            skip = $scope.list.length
                            if (data && !data.length) {
                                $scope.dateclick = false
                                $scope.loading = false;
                            }

                        })
                    }


                }
                else {

                }
            }
        });


        $scope.find_data = function () {
            if (isNullOrEmpty($scope.date.startdate) && isNullOrEmpty($scope.date.enddate)) {
                $scope.strequier = true;
                $scope.edrequier = true;
                $scope.msg = "field required";
                return;
            } else {

                if (!validation.isStGreaterEd($scope.date.startdate, $scope.date.enddate)) {
                    $scope.msg = "End date should be greater than start date";
                    $scope.date.startdate = "";
                    $scope.date.enddate = "";

                    return;
                }
            }
            if (isNullOrEmpty($scope.date.startdate)) {
                $scope.strequier = true;
                $scope.msg = "Start date required";
                return;
            }
            if (isNullOrEmpty($scope.date.enddate)) {
                $scope.edrequier = true;
                $scope.msg = "end date required";
                return;
            }

            if (!validation.testDate($scope.date.startdate) || !validation.testDate($scope.date.enddate)) {
                if (!validation.testDate($scope.date.startdate)) {
                    $scope.strequier = true;
                }
                if (!validation.testDate($scope.date.enddate)) {
                    $scope.edrequier = true;
                }
                $scope.msg = "Please enter valid date";
                return;
            }
            var date = $scope.date;
            $scope.query = {}
            $scope.queryBy = '$';
            $scope.list = [];
            $scope.showbutton = true;
            var skip = 0;
            getSearchData('find_date', $scope.searchvalue, skip, limit, date, function (data) {
                $scope.list = $scope.list.concat(data);
                skip = $scope.list.length
                if (data && !data.length) {
                    $scope.showbutton = false;
                }
            })


        }
        $scope.findall = function () {
            $scope.strequier = false;
            $scope.edrequier = false;
            $scope.msg = "";
            $scope.showbutton = true;
            var skip = 0;
            getSearchData('search', $scope.searchvalue, skip, limit, 'date', function (data) {

                $scope.list = data;
                $scope.date = "";
            })

        }

    }
    $scope.sendSeoUrlParam = function (seourl) {

        if (seourl) {
            var urls = seourl.split('/');

            if (urls && urls.length) {
                var mainSeoUrl = urls.pop()

                $state.go('description', { obj: mainSeoUrl });
            }
        }
    }

});
app.controller("uploadFormCtrl", function ($rootScope, $state) {
    if ($rootScope.currentUser.isLoggedIn && $rootScope.currentUser.user) {
        if ($rootScope.currentUser.user.role != 'admin') {
            $state.go("home")
        }
    }
});
app.controller('fileUploadCtrl', function (sessionService, requestupdateClip, catService, $location, Upload, $cookies, $window, $state, $rootScope, $scope, $http, $timeout, $templateCache, $compile, $filter) {
    //07-12-16
    $scope.cdate = $filter('date')(new Date(), 'MM-dd-yyyy');
    $scope.time = $filter('date')(new Date(), 'HH:mm');
    //on click in edit mode open time dialogue


    $scope.fields = {};
    $scope.fileaudio = {};
    $scope.imgfileimage = {};
    $scope.upload_form11 = {};
    $scope.datetime = '';
    $scope.submitted = false;
    $scope.upload_form = {};
    $scope.shoowfiledata = {
        showfile: false,
        showaudio: false
    }
    var editResponse1 = '',
        editResponse2 = '',
        resp1 = {},
        resp2 = {};
    vm = this
    if ($location.path() == '/adminpage') {
        $scope.uploadhit = true;
    }
    else if ($location.path() == '/uploadform') {
        $scope.uploadhit = false;
    }
    $scope.updateclip = function (id) {
        $scope['uploadcurrentData'] = {};

        $scope.fields = {};
        $http.get('views/updateclip.html').success(function (data2) {
            $scope['dataids'] = id;

            requestupdateClip.recent(id).then(function (data) {
                $scope['uploadcurrentData'] = data;


                $scope['currentCatId'] = data.categoryId;

                $(document.body).append($compile(data2)($scope));
                $('#updateclip11').modal('show');
                $('#timepicker2').timepicker({
                    timeFormat: 'h:mm p',
                    minuteStep: 5,
                    //defaultTime: def,
                    showMeridian: false
                })
                catService.getCat().then(function (data) {


                    if ($scope['uploadcurrentData']) {
                        $scope.fields = cloneData($scope['uploadcurrentData']);
                        //01-12-2016
                        if ($scope.fields.youtubeUrl) {
                            if ($scope.fields.youtubeUrl.indexOf('embed')) {
                                $scope.fields.youtubeUrl.replace('embed/', 'watch?v=');
                            }
                        }
                        if ($scope.fields.postedOn) {
                            var dt = new Date($scope.fields.postedOn);
                            dt.setHours(dt.getHours() - 5);
                            dt.setMinutes(dt.getMinutes() - 30);

                            $scope.fields.postedOn = $filter('date')(new Date(dt), 'yyyy-MM-dd HH:mm:ss');

                        }
                        //01-12-2016 closed
                        //07-12-16

                        $scope.cdate = $filter('date')(new Date($scope.fields.postedOn), 'MM-dd-yyyy');
                        $scope.time = $filter('date')(new Date($scope.fields.postedOn), 'HH:mm');

                        /*$scope.postedOn=$scope.cdate+' '+$scope.time+':00';
                         $scope.postedOn=$filter('date')(new Date($scope.postedOn),'yyyy-MM-dd HH:mm:ss');
                         console.log('<<<<<<_____',$scope.postedOn);*/
                        //07-12-16 end
                    }
                    $scope.cat = data;
                    var id = $scope['currentCatId']
                    $scope.selectedCategory = data[id-1].name;
                    if ($scope.fields && $scope.fields.url) {
                        var aUrl = $scope.fields.url.split('s/');
                        //    console.log("aUrl>>>>>>>",aUrl)
                        $scope.fields['url'] = './uploads/'+ aUrl[1];

                        $scope.showaudio = true;
                        $scope.shoowfiledata['showaudio'] = true

                    }
                    if ($scope.fields && $scope.fields.imageurl) {
                        var iUrl = $scope.fields.imageurl.split('s/');
                        $scope.fields['imageurl'] = './uploads/'+iUrl[1]
                        $scope.shoowfiledata['showfile'] = true

                    }


                })

            })

        });

    }
    $scope.getId = function (url) {
        var regExp = /^.*(youtube\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
        var match = url.match(regExp);

        if (match && match[2].length == 11) {
            return match[2];
        } else {
            return 'error';
        }
    }
    if ($location.path() == '/uploadform') {
        catService.getCat().then(function (d) {
            $scope.cat = d;
        });
    }
    function uploadAdminFiles(file, next) {
        Upload.upload({
            url: UPLOAD_URL + 'upload',
            data: { file: file }
        }).then(function (data) {
            next(data);
        }, function (error) {
            next(error);
        }, function (evt) {

            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $scope.progress = 'progress: ' + progressPercentage + '% ';

        })
    }
    $scope.editData = function () {

        var seotext = '';

        var changeDetect = false,
            mediachange = false,
            imagechange = false;

        if (Object.keys($scope.fields).length) {
            for (var key in $scope.fields) {
                if ($scope['uploadcurrentData']) {
                    if ($scope['uploadcurrentData'][key] != $scope.fields[key]) {

                        if (key != 'imageurl' && key != 'url') {

                            changeDetect = true;
                        }
                        if (key == 'name') {
                            if ($scope.fields.name) {
                                var name = $scope.fields.name.split(' ');
                                if (name && name.length) {
                                    var totallength = name.length;
                                    var count = 0;
                                    name.forEach(function (textdata) {
                                        if (count == totallength - 1) {
                                            seotext = seotext.concat(textdata);
                                        }
                                        else {
                                            seotext = seotext.concat(textdata + '-');
                                        }
                                        count++;

                                    })
                                }

                            }
                            var datetimestamp = Date.now();
                            if (seotext) {
                                seotext = seotext.concat(datetimestamp);
                                //  seotext = 'http://www.mnpoliceclips.com/' + seotext + 'ebut.html';
                                seotext = '/' +seotext.replace(/[^a-zA-Z ]/g, "-") + '.html';

                            }
                            $scope.fields['seoUrl'] = seotext;

                        }

                    }
                }
            }

        }
        if ($scope.fileaudio || $scope.imgfileimage) {
            if ($scope.fileaudio && $scope.fileaudio.name) {
                if ($scope.fields && $scope.fileaudio) {
                    if ($scope.fields.url != $scope.fileaudio.name) {
                        mediachange = true;
                        changeDetect = true;
                        //    $scope.shoowfiledata['showaudio'] = false

                    }
                }

            }
            if ($scope.imgfileimage && $scope.imgfileimage.name) {
                if ($scope.fields && $scope.imgfileimage) {
                    if ($scope.fields.imageurl != $scope.imgfileimage.name) {
                        imagechange = true;
                        changeDetect = true;
                        $scope.shoowfiledata['showfile'] = false
                    }
                }
            }
        }

        $scope.fields['postedOn'] = $scope.cdate + ' ' + $scope.time + ':00';
        $scope.fields['postedOn'] = $filter('date')(new Date($scope.fields['postedOn']), 'yyyy-MM-dd HH:mm:ss');
        $scope.fields['postedBy'] = $rootScope.currentUser.user.id;
        $scope.fields['uploaderName'] = $rootScope.currentUser.user.email;
        if($scope.fields['url']){
            $scope.fields['url'] = $scope.fields['url'];
        }
        if($scope.fields['imageurl']){
            $scope.fields['imageurl'] =  $scope.fields['imageurl'];
        }


        // $scope.shoowfiledata = {
        //  showfile: false,
        //showaudio: false
        //};
        if (!isNullOrEmpty($scope.youtubeUrl)) {
            var youtubeId = $scope.getId($scope.youtubeUrl);
            $scope.youtube_url = "https://www.youtube.com/embed/" + youtubeId;
            $scope.fields['youtube_url'] = $scope.youtube_url;

        }
        if ($scope.fields.youtubeUrl) {
            if ($scope.fields.youtubeUrl.indexOf('embed') >= 0) {
                //$scope.fields.youtubeUrl.replace('embed/','watch?v=');
            }
            else {
                var youtubeId = $scope.getId($scope.fields.youtubeUrl);
                $scope.fields.youtubeUrl = "https://www.youtube.com/embed/" + youtubeId;
            }
        }
        if (changeDetect) {
            if (imagechange && mediachange) {
                uploadAdminFiles($scope.fileaudio, function (resp) {
                    editResponse1 =   resp.data.fileName;
                    uploadAdminFiles($scope.imgfileimage, function (resp) {
                        editResponse2 = resp.data.fileName;
                        $scope.fields['url'] = "./uploads/" + editResponse1;
                        $scope.fields['imageurl'] = "./uploads/" + editResponse2;

                        editcompleteData($scope.fields)
                    })
                })
            }
            else if (!imagechange && mediachange) {
                uploadAdminFiles($scope.fileaudio, function (resp) {
                    editResponse1 = resp.data.fileName;
                    $scope.fields['url'] = "./uploads/" + editResponse1;
                    editcompleteData($scope.fields)
                })
            }
            else if (imagechange && !mediachange) {
                uploadAdminFiles($scope.imgfileimage, function (resp) {
                    editResponse2 = resp.data.fileName;
                    $scope.fields['imageurl'] = "./uploads/" + editResponse2;
                    editcompleteData($scope.fields)
                })

            }
            else if (!imagechange && !mediachange) {

                editcompleteData($scope.fields);

            }

        }
        else
        {

        }
        function editcompleteData() {
            $scope.fields['postedBy'] = $rootScope.currentUser.user.id;
            $scope.fields['uploaderName'] = $rootScope.currentUser.user.email;

            $http.patch(BASE_URL + "Transmissions/" + $scope.fields['id'], $scope.fields)
                .then(function (data, status, headers, config) {

                    document.getElementById("imgfile").value = "";
                    document.getElementById("audiofile").value = "";
                    $scope['uploadcurrentData'] = {};
                    $('#updateclip11').modal('hide');



                    var element = document.getElementById("updateclip11");
                    element.parentNode.removeChild(element);
                    $scope.fields = {};
                    $scope.adminData = {}
                    swal({
                        title: "Success",
                        text: 'Success fully Edit ',
                        type: "success",
                        timer: 2000,
                        animation: "slide-from-top",
                        showOkButton: false
                    });
                    $scope.$parent.$parent.updateEdit();

                    //window.location.reload()

                });
        }


    }
    $scope.cancelEdit = function () {
        $scope['uploadcurrentData'] = {};
        $('#updateclip11').modal('hide');
        var element = document.getElementById("updateclip11");
        element.parentNode.removeChild(element);

    }
    vm.submit = function () {

        //07-12-16
        // $scope.postedOn = $scope.cdate + ' ' + $scope.time + ':00';
        var param = $scope.cdate.split('-');
        $scope.postedOn = param[2]+'-'+param[0]+'-'+param[1]+ ' ' + $scope.time + ':00';
        // $scope.postedOn = $filter('date')(new Date($scope.postedOn), 'yyyy-MM-dd HH:mm:ss');

        //07-12-16 end

        //$scope.media_selected=false;

        if ($scope.upload_form.$invalid) {
            //$scope.media_selected=true;
            $scope.submitted = true;
            $scope.file = '';

            return;
        }
        if ($scope.media_type == 'audio' || $scope.media_type == 'video') {
            if ($scope.file && $scope.imgfile) {
                uploadAdminFiles($scope.file, function (resp) {
                    // console.log(">>>>>>>>>>>>>"+resp);
                    resp1 = resp;
                    uploadAdminFiles($scope.imgfile, function (resp) {
                        resp2 = resp;
                        submitMediaForm('submit', resp1, resp2, function () {

                        })
                    })
                })

            }
            else if ($scope.file && !$scope.imgfile) {
                uploadAdminFiles($scope.file, function (resp) {
                    resp1 = resp;
                    submitMediaForm('submit', resp1, {}, function () {

                    })

                })
            }
            else if (!$scope.file && $scope.imgfile) {
                uploadAdminFiles($scope.imgfile, function (resp) {
                    resp2 = resp;
                    submitMediaForm('submit', {}, resp2, function () {

                    })
                })

            }
        }
        else {

            if (!isNullOrEmpty($scope.youtube_url)) {
                var youtubeId = $scope.getId($scope.youtube_url);
                $scope.youtube_url = "https://www.youtube.com/embed/" + youtubeId;
            }
            if ($scope.imgfile) {
                uploadAdminFiles($scope.imgfile, function (resp) {
                    resp2 = resp;
                    submitMediaForm('submit', {}, resp2, function () {

                    })
                })
            }
            else {
                submitMediaForm('submit', {}, {}, function () {

                })
            }

        }
        function submitMediaForm(type, resp1, resp2, next) {
            var dataobject = {
                id: 0,
                name: $scope.name,
                categoryId: $scope.selected_category,
                description: $scope.description,
                type: $scope.media_type,
                length: null,
                url: '',
                imageurl: '',
                postedOn: $scope.postedOn,
                postedBy: $rootScope.currentUser.user.id,
                uploaderName: $rootScope.currentUser.user.email,
                seoUrl: "",
                youtubeUrl: $scope.youtube_url,
                active: 1
            }

            if (resp1 && resp1.data && resp1.data.fileName) {

                dataobject['url'] = "./uploads/" + resp1.data.fileName
            }
            if (resp2 && resp2.data && resp2.data.fileName) {

                dataobject['imageurl'] = "./uploads/" + resp2.data.fileName
            }
            var seotext = '';
            if ($scope.name) {
                var name = $scope.name.split(' ');

                if (name && name.length) {
                    var totallength = name.length;
                    var count = 0;
                    name.forEach(function (textdata) {
                        if (count == totallength - 1) {
                            seotext = seotext.concat(textdata);
                        }
                        else {
                            seotext = seotext.concat(textdata + '-');
                        }
                        count++;

                    })
                }

            }
            var datetimestamp = Date.now();
            if (seotext) {
                seotext = seotext.concat(datetimestamp);
                //  seotext='http://www.mnpoliceclips.com/'+seotext+'.html';
                seotext = '/'+seotext.replace(/[^a-zA-Z ]/g, "-") + '.html';

            }
            dataobject['seoUrl'] = seotext;
            var Parameter = JSON.stringify(dataobject);
            $http.post(BASE_URL + "Transmissions", Parameter)
                .then(function (data, status, headers, config) {

                    $scope.medialoading = false;
                    $scope.submitted = false;
                    $scope.name = "";
                    $scope.selected_category = "";
                    $scope.description = "";
                    $scope.media_type = "";
                    $scope.date = "";
                    $scope.datetime = "";
                    $scope.file = "";
                    $scope.youtube_url = "";
                    $scope.progress = '';
                    document.getElementById("imgfile").value = "";
                    document.getElementById("audiofile").value = "";
                    swal({
                        title: "Success",
                        text: 'Successfully Uploaded',
                        type: "success",
                        timer: 2000,
                        animation: "slide-from-top",
                        showOkButton: false
                    });
                    $state.go('adminpage');
                });

        }


    }
    $scope.clear = function () {
        document.getElementById('uploadform').reset();
        $scope.up.file = "";
        $scope.up.imgfile = "";
        $state.go('/adminpage');
    }
    $scope.validateMediaType = function () {

        if ($scope.youtube_url.indexOf('www.youtube.com') >= 0) {
            $scope.media_selected = false;
        }
    }


});
app.controller("passCtrl", function ($scope) {
    $scope.openPasmodal = function () {
        $('#navbar').toggleClass('in');
        $('.navbar-toggle.ResposiveNavBtn').toggleClass('collapsed');
        $("#changePass").modal()
    }
})
app.controller("userslistCtrl", function ($scope, adapter, $http, $compile,$window) {
    $scope.query = {}
    $scope.queryBy = '$';
    $scope.list = [];
    $scope.noRecords = false;
    $scope.showbutton = true;
    var limit = 50;
    var skip = 0;
    getSearchData('loadmore', skip, limit, 'user', function (data) {

        $scope.list = data;
        if (data && !data.length) {
            $scope.showbutton = false;
        }

    })
    function getSearchData(val, skip1, limit, user, next) {

        var request = {
            method: 'get',
            command: 'usersRegistartions',
            query: {}
        };
        if (val == "loadmore") {

            request.query.filter = {
                limit: limit,
                skip: skip1
            }



        }
        if (val == "finduser") {

            if (!isNullOrEmpty(user.email)) {
                request.query.filter = {
                    "where": {

                        "email": {
                            "regexp": "/" + user.email + "*/"
                        }
                    }

                }
            }
            if (!isNullOrEmpty(user.name)) {
                request.query.filter = {
                    "where": {

                        "firstName": {
                            "regexp": "/" + user.name + "*/"
                        }
                    }

                }
            }
            if (!isNullOrEmpty(user.phoneno)) {
                request.query.filter = {
                    "where": {

                        "phoneNo": {
                            "regexp": "/" + user.phoneno + "*/"
                        }
                    }

                }
            }
        }

        adapter.call(request)
            .then(function (data) {
                if (data && data.length) {

                    next(data)
                    $scope.user = {}
                }
                else {
                    next(data)
                }

            }, function (error) {
                next()
                $scope.user = {}
            })

    }

    angular.element($window).bind('scroll', function () {
        var scrollTop = $(window).scrollTop();
        var docHeight = $(document).height();
        var winHeight = $(window).height();
        var scrollPercent = (scrollTop) / (docHeight - winHeight);
        var scrollPercentRounded = Math.round(scrollPercent*100);
        if (scrollPercentRounded >= 95 ) {
            $('#back-to-top').fadeIn();
            if ($scope.showbutton == true) {
                getSearchData('loadmore', $scope.list.length, limit, 'user', function (data) {

                    $scope.list = $scope.list.concat(data);
                    skip = $scope.list.length
                    if (data && !data.length) {
                        $scope.showbutton = false;
                    }
                })
            }
            else {

            }
        }
        else{
            $('#back-to-top').fadeOut();
        }
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $(window).scrollTop(0);
            // $('#back-to-top').fadeOut();
        });

        $('#back-to-top').tooltip('show');
    });

    //$('#scrollingDiv4').bind('scroll', function () {
    //    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
    //
    //
    //        if ($scope.showbutton == true) {
    //            getSearchData('loadmore', $scope.list.length, limit, 'user', function (data) {
    //
    //                $scope.list = $scope.list.concat(data);
    //                skip = $scope.list.length
    //                if (data && !data.length) {
    //                    $scope.showbutton = false;
    //                }
    //            })
    //        }
    //        else {
    //
    //        }
    //
    //    }
    //});

    $scope.finduser = function (user) {

        $scope.noRecords = false;
        getSearchData('finduser', 'skip', 'limit', user, function (data) {

            $scope.list = data;
            if (data && data.length) {
                $scope.showbutton = false;
            }
            if(isNullOrEmpty(data)){
                $scope.noRecords = true ;
            }

        })

    }
    $scope.allList = function () {
        $scope.showbutton = true;
        $scope.noRecords = false ;
        var skip = 0;
        getSearchData('loadmore', skip, limit, 'user', function (data) {

            $scope.list = data;
            if (data && !data.length) {
                $scope.showbutton = false;
            }

        })
    }
    $scope.changeStatus = function (userId, activity) {

        var request = {};
        if (activity && activity == 'Active') {
            request = {
                "active": 1
            }
        }
        else if (activity && activity == 'Inactive') {
            request = {
                "active": 0
            }
        }

        $http.patch(BASE_URL + "usersRegistartions/" + userId, request)
            .then(function (data, status, headers, config) {

                $scope.accountInfo = data.data;

                swal({
                    title: "Success",
                    text: 'User Status changed ',
                    type: "success",
                    timer: 2000,
                    animation: "slide-from-top",
                    showOkButton: false
                });

            });

    }
    $scope.addUser = function (user) {
        $scope.user = user;
        $http.get("views/adduser.html").success(function (data2) {
            $(document.body).append($compile(data2)($scope));
            $('#addUser').modal('show');
        })
    }
    $scope.clearselectuser = function () {
        $scope.user = "";
    }
    $scope.updateEdit = function () {
        var skip = 0;
        getSearchData('loadmore', skip, limit, 'user', function (data) {

            $scope.list = data;
            if (data && !data.length) {
                $scope.showbutton = false;
            }

        })
    }
})
app.controller("accountsetCtrl", function ($scope, $http, $filter, $q, $rootScope) {
    $scope.accountInfo = {};
    var currentUser = JSON.parse(localStorage.getItem("cUser"));
    var id = currentUser.user.id;
    $scope.accountInfo = currentUser.user;

    $scope.updateInfo = function () {

        $scope.accountInfo['createdOn'] = new Date('MM-dd-yyyy')
        $http.patch(BASE_URL + "usersRegistartions/" + id, $scope.accountInfo)
            .then(function (data, status, headers, config) {


                $scope.accountInfo = data.data;
                $rootScope.setCurrentUser(data.data,function(){})
                swal({
                    title: "Success",
                    text: 'Success fully Edit ',
                    type: "success",
                    timer: 2000,
                    animation: "slide-from-top",
                    showOkButton: false
                });
            });
    }
})

app.controller("contactusctrl", function ($scope, $http, adapter, $filter) {
    $scope.onsucess = false;
    $scope.submitMail = function () {
        $scope.submitted = true;
        $scope.fields['id'] = 0;
        $scope.fields['active'] = 1;
        $scope.fields['createdOn'] = $filter('date')(new Date(), 'MM-dd-yyyy');
        var request = {
            method: 'post',
            command: 'Contacts',
            data: $scope.fields

        };
        adapter.call(request).then(function (contact) {
            var mailids = ['mnpoliceclips@gmail.com', "pankajmalik110@gmail.com"];
            /* if($scope.fields && $scope.fields.email){
             mailids.push($scope.fields.email)
             }*/
            var data = $.param({
                fromEmail: "admin@admin.com",
                toEmail: mailids,
                name: $scope.fields['name'],
                message: 'Got Email',
                templateData: $scope.fields,
                template: 'contactemail'
            })
            //data=JSON.parse(data)

            $http.post('/sendEmail', data, {
                headers: {

                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).success(function (data, status) {
                $scope.onsucess = false;
                swal({
                    title: "Success!",
                    text: "Thanks for Contact To us",
                    type: "success",
                    timer: 1000
                });
                $scope.fields = {};


            })


        })
            .catch(function (error) {
                if (error && error.error) {
                    if (error.error && error.error.statusCode == 413) {

                    }


                }
            })
    }
})
app.controller('paymentgateway', function ($scope, $rootScope, $location, $cookies, adapter, $http, $compile, $templateCache, $timeout) {
    $scope.plans = {
        'month': 1
    }
    var amount = {
        "1": 4.95,
        "12": 49.5
    }
    $scope.autodebit = {
        "status": 'NO'
    }
    $scope.paymentSub = function () {
        var payment = {
            method: 'get',
            command: 'UsersSubscriptions/makePayment',
            query: {
                amount: amount[$scope.plans.month],
                userId: $rootScope.currentUser.user.id,
                month: $scope.plans.month,
                status: $scope.autodebit.status

            }

        }
        adapter.call(payment).
            then(function (data) {
                if (data) {
                    window.location.href = data;
                }

            }).
            catch(function (error) {

            })
    }
})
app.controller('viewincidentCtrl', function (catService,$filter, $rootScope, $http, $timeout, $state, recentClips, $scope, adapter, validation, reqCategory_data, $sce) {
    $scope.sortName = "";
    $scope.slectedCategory = 'Recent Audio Feeds';
    $scope.strequier = false;
    $scope.edrequier = false;
    $scope.searchval = false;
    $scope.query = {}
    $scope.queryBy = '$';
    $scope.list = [];
    var searchin ="";
    var limit = 50;
    var skip = 0;
    var loadMore = true;
    $scope.noClip = false ;
    $scope.showMore = function (index) {

        var disc = document.getElementById("disc-" + index);
        var see = document.getElementById("see-" + index);
        disc.className = 'discDiv show';
        see.className = 'hide';
    };
    $rootScope.$on('stopAudio', function(event, mass) {
        //  console.log("play..",!$scope.audio.paused);
        if (!$scope.audio.paused) {
            $scope.audio.pause();

        }
    });
    $scope.parseDate = function(current, pre, index)
    {
        // console.log(index);
        var now = new Date;
        var date = new Date(current);
        var currentpasseddate = getFormateDate(current);
        if(index == 0)
        {

            var utc_timestamp = new Date();
            utc_timestamp = utc_timestamp.toISOString(). replace(/T/, ' ').replace(/\..+/, '').substr(0,10).trim();
            if(currentpasseddate == utc_timestamp)
                return 'Today at ';
            else
                return current;
        }
        else{
            var prepasseddate = getFormateDate(pre);
            var currentdate = getFormateDate(current);
            if(prepasseddate == currentdate)
                return '';
            else
                return current;

        }
    }
    function getFormateDate(value)
    {
        var date = new Date(value);
        var prepasseddate = new Date(date.getUTCFullYear(),date.getUTCMonth(),date.getUTCDate(), 0,0,0);
        return date.toISOString(). replace(/T/, ' ').replace(/\..+/, '').substr(0,10).trim();
    }

    $scope.onSelects = function(selection) {
        var propertyName = Object.keys(selection)

        if(propertyName[0] == 'city'){
            $scope.searchitem.city = selection.city ;
        }
        else{
            $scope.searchitem.event_type = selection.event_type ;
        }

        $scope.selectedData = selection;
    };



    function city() {
        var url = BASE_URL +'Transmissions/city'
        $http({ method: 'GET', url: url }).
            success(function (data, status, headers, config) {
                $scope.city = data;

            }).
            error(function (data, status, headers, config) {

            });

    }

    function event() {
        var url = BASE_URL + 'Transmissions/event'
        $http({ method: 'GET', url: url }).
            success(function (data, status, headers, config) {
                $scope.event = data;

            }).
            error(function (data, status, headers, config) {

            });

    }
    city();
    event();

    $scope.searchitem = {
        "startdate": "",
        "enddate": "",
        "city": "",
        "event_type": ""
    }
    var isFilter = false;
    var keyNamePre = ""
    $scope.selectedkey = "Sort By";
    var user = JSON.parse(localStorage.getItem("cUser"));
    if (isNullOrEmpty(user)) {
        $scope.isLoggIn = false;

    } else {
        $scope.isLoggIn = user.isLoggedIn;
    }
    $scope.sort = function (name,keyname) {
        var getClassdown = document.getElementsByClassName('glyphicon glyphicon-menu-down');
        var getClassup = document.getElementsByClassName('glyphicon glyphicon-menu-up');
        var dataa = $scope.list;
        $scope.list ="";
        if (getClassdown.length > 0) {
            getClassdown[0].className = "";

        }
        if (getClassup.length > 0) {
            getClassup[0].className = "";

        }
        $scope.sortKey = keyname;


        //console.log("$scope.reverse",$scope.reverse)
        if (keyNamePre == keyname) {
            $scope.reverse = !$scope.reverse;
            if ($scope.reverse) {
                document.getElementById(keyname).childNodes[0].childNodes[1].className = 'glyphicon glyphicon-menu-down pdl-80';

            }
            else {

                document.getElementById(keyname).childNodes[0].childNodes[1].className = 'glyphicon glyphicon-menu-up pdl-80';
            }

        }
        else {
            $scope.reverse = false;

            document.getElementById(keyname).childNodes[0].childNodes[1].className = 'glyphicon glyphicon-menu-down pdl-80';
        }

        $timeout(function () {
            $scope.showDropdown = false;
            keyNamePre = keyname;
            $scope.selectedkey = name;
            $scope.list = dataa;

            $("#scrollingDiv").scrollTop(0);
        }, 100);



    }

    getSearchData('allData', 'id', "", skip, limit, function (data) {
        //$scope.list = $sce.trustAsHtml(data);

        $scope.list = data;


        if (isNullOrEmpty(data)) {
            loadMore = false;
            $scope.noClip = true;
        }
    })

    $scope.selected = null;
    function getSearchData(val, id, searchitem, skip1, limit, next) {

        if (val == 'allData') {

            request = {
                method: 'get',
                command: 'Transmissions',
                query: {
                    filter: {
                        where: { "postid": { "neq": null } },
                        'group by': "postid,id",
                        order: "id DESC",
                        limit: limit,
                        skip: skip1
                    }
                }

            };
        }

        if (val == 'filter') {

            request = {
                method: 'get',
                command: 'Transmissions',
                query: {}
            };
            request.query.filter = {
                "where": filterQuery(searchitem),
                'group by': "postid,id",
                order: "id DESC",
                limit: limit,
                skip: skip1
            };




        }

        adapter.call(request)
            .then(function (data) {

                $scope.skip = data.length;
                if (data && data.length ) {
                    //  loadMore = true;
                    $scope.noClip = false;
                    var getData = fbPosts(data);

                    next(getData);

                }

                else {
                    // loadMore = false;
                    // $scope.noClip = true;

                    next(data)
                }

            }, function (error) {
                next()
            })

    }

    function fbPosts(data) {

        var fbArray = [[]];
        var ids = getId(data);
        for (var i = 0; i < ids.length; i++) {
            var k = 0;
            var arr = [];
            for (var j = 0; j < data.length; j++) {
                if (data[j].postid == ids[i]) {

                    arr.push(data[j]);

                }
            }
            fbArray[i] = arr;
        }

        return fbArray;
    }
    function getId(data) {
        var postids = []
        for (var i = 0; i < data.length; i++) {
            if (postids.indexOf(data[i].postid) == -1 && data[i].postid != null) {
                postids.push(data[i].postid);
            }
        }

        return postids
    }

    function filterQuery(searchitem) {
        var query = "";
        var sDate ="";
        var eDate = "";
        if (!isNullOrEmpty(searchitem.enddate) && !isNullOrEmpty(searchitem.startdate)) {
            if (validation.testDate(searchitem.enddate) && validation.testDate(searchitem.startdate)) {
                var startdate = new Date(searchitem.startdate);
                var enddate = new Date(searchitem.enddate);
                sDate = $filter('date')(new Date(startdate), 'yyyy-MM-dd');
                eDate = $filter('date')(new Date(enddate), 'yyyy-MM-dd 23:59:59');

            }
        }
        if (!isNullOrEmpty(searchitem.startdate) && !isNullOrEmpty(searchitem.enddate) && !isNullOrEmpty(searchitem.city) && !isNullOrEmpty(searchitem.event_type)) {

            query = {
                "and"
                    :
                    [
                        {
                            "postedOn": {
                                "between": [sDate, eDate]
                            }
                        },
                        {
                            "city": searchitem.city
                        },

                        {
                            "event_type": searchitem.event_type
                        }, {
                        "postid": { "neq": null }
                    }

                    ]
            }
        }
        else if (isNullOrEmpty(searchitem.startdate) && isNullOrEmpty(searchitem.enddate) && !isNullOrEmpty(searchitem.city) && !isNullOrEmpty(searchitem.event_type)) {

            query = {
                "and": [

                    {
                        "city": searchitem.city
                    },

                    {
                        "event_type": searchitem.event_type
                    }
                ]
            }
        }
        else if (isNullOrEmpty(searchitem.startdate) && isNullOrEmpty(searchitem.enddate) && isNullOrEmpty(searchitem.city) && !isNullOrEmpty(searchitem.event_type)) {
            query = {
                "event_type": searchitem.event_type

            };
        }
        else if (isNullOrEmpty(searchitem.startdate) && isNullOrEmpty(searchitem.enddate) && !isNullOrEmpty(searchitem.city) && isNullOrEmpty(searchitem.event_type)) {
            query = {


                "city": searchitem.city

            };
        }
        else if (!isNullOrEmpty(searchitem.startdate) && !isNullOrEmpty(searchitem.enddate) && isNullOrEmpty(searchitem.city) && isNullOrEmpty(searchitem.event_type)) {
            query = {

                "postedOn": {
                    "between": [sDate, eDate]
                }

            };
        }
        else if (!isNullOrEmpty(searchitem.startdate) && !isNullOrEmpty(searchitem.enddate) && !isNullOrEmpty(searchitem.city) && isNullOrEmpty(searchitem.event_type)) {
            query = {
                "and": [{
                    "postedOn": {
                        "between": [sDate, eDate]
                    },
                    "city": searchitem.city
                }

                ]


            };
        }
        else if (!isNullOrEmpty(searchitem.startdate) && !isNullOrEmpty(searchitem.enddate) && isNullOrEmpty(searchitem.city) && !isNullOrEmpty(searchitem.event_type)) {
            query = {
                "and": [{
                    "postedOn": {
                        "between": [sDate, eDate]
                    },
                    "event_type": searchitem.event_type
                }

                ]


            };
        }
        return query;

    }
    $scope.onchangeDate = function (type, value) {

        if (!isNullOrEmpty(value)) {
            $scope.strequier = false;
            $scope.edrequier = false;
            $scope.searchval = false;
            $scope.msg = "";

        }

        if (type == "cities") {
            $scope.searchitem.city = value;
        }
        if (type == "event") {
            $scope.searchitem.event_type = value;
        }

    }

    $scope.clearInput = function() {
        $scope.$broadcast('autocomplete:clearInput');
    };
    $scope.reset = function () {
        // $("#scrollingDiv").scrollTop();
        var skip = 0;
        var limit = 50;
        searchin = "";
        $scope.msg = "";
        $scope.strequier = false;
        $scope.edrequier = false;
        $scope.searchval = false;
        $scope.list = "";
        $scope.noClip = false;
        loadMore = true;
        isFilter = false;


        var cc = document.getElementsByClassName('discDiv');
        cc.className = 'discDiv show'
        $scope.clearInput();

        getSearchData('allData', 'id', "", skip, limit, function (data) {
            $scope.selectedkey = "Sort By";
            $scope.sortKey = "postid";
            $scope.list = data;

            $scope.searchitem = {
                "startdate": "",
                "enddate": "",
                "city": "",
                "event_type": ""
            }


            if (isNullOrEmpty(data)) {
                loadMore = false;
            }
        })

    }
    $scope.audio = new Audio();
    var preBtn = "";
    $scope.playAudio = function (parentindex, childindex, url, btnObj) {


        var playBtn = 'play-' + parentindex + '-' + childindex;
        var pButton = document.getElementById(playBtn);

        if (preBtn == pButton) {
            if ($scope.audio.paused) {
                $scope.audio.play();
                // pButton.className = "";
                pButton.className = "playbtn stop  ";

            } else {
                $scope.audio.pause();
                //  pButton.className = "";
                pButton.className = "playbtn play";
            }
        }
        else {

            $scope.audio.src = url;
            $scope.audio.play();
            if (preBtn != "") {
                // preBtn.className = "";
                preBtn.className = "playbtn play";
            }

            //  pButton.className = "";
            pButton.className = "playbtn stop ";

        }


        preBtn = pButton;
    }
    $scope.audio.addEventListener('ended', function () {
        preBtn.className = "";
        preBtn.className = "playbtn play";
        preBtn = "";
        $scope.$apply();  // <<<<

    });
    $('#scrollingDiv').bind('scroll', function () {

        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight && $(this)[0].scrollHeight >1) {
            var h =  $(this)[0].scrollHeight-50;

            if (loadMore == false) {

            }
            if (loadMore) {
                if (isFilter) {
                    var date = $scope.date;
                    getSearchData('filter', 'id', $scope.searchitem, $scope.skip, limit, function (data) {
                        $scope.list = $scope.list.concat(data);
                        skip = $scope.list.length
                        if (data.length < 1 || data[0].length < 50 ) {
                            loadMore = false;
                        }

                    })
                }

                else{
                    var date = $scope.date;
                    getSearchData('allData', 'id', "", $scope.skip, limit, function (data) {
                        var d = $scope.list.concat(data);

                        $scope.list = "";
                        $timeout(function(){
                            $scope.list = d ;
                        },10)
                        $timeout(function(){
                            $('#scrollingDiv').scrollTop(h)
                        },15)
                        skip = $scope.list.length;
                        if (data.length < 1 || data[0].length < 50 ) {
                            loadMore = false;
                        }

                    })
                }

            }
        }
    })


    $scope.sendSeoUrlParam = function (seourl) {

        if (seourl) {
            var urls = seourl.split('/');

            if (urls && urls.length) {
                var mainSeoUrl = urls.pop()

                $state.go('description', { obj: mainSeoUrl });
            }
        }
    }

    $scope.findall = function () {

        var limit = 50;
        var skip = 0;
        getSearchData('allData', 'id', "", skip, limit, function (data) {
            $scope.list = data;
            if (isNullOrEmpty(data)) {
                $scope.showbutton = true;
            }
        })

    }

    $scope.find = function (id, index, name) {
        $scope.slectedCategory = name;
        $scope.selected = index;
        $scope.catId = id;
        var skip = 0
        var limit = 50
        getSearchData('catData', id, "", skip, limit, function (data) {
            $scope.list = data;
            if (isNullOrEmpty(data)) {
                $scope.showbutton = true;
            }
        })
    }
    $scope.filterSearch = function (searchitem) {

        skip = 0;


        if ((isNullOrEmpty(searchitem.startdate) && isNullOrEmpty(searchitem.enddate)) && isNullOrEmpty(searchitem.city)&& isNullOrEmpty(searchitem.event_type) ) {


            $scope.strequier = true;
            $scope.edrequier = true;
            $scope.searchval = true;

            $scope.msg = "Please enter values";
            return;
        }
        else {
            $scope.searchval = false;
            $scope.strequier = false;
            $scope.edrequier = false;
            $scope.msg = "";


        }
        if (isNullOrEmpty(searchitem.startdate) && !isNullOrEmpty(searchitem.enddate)) {

            $scope.strequier = true;
            $scope.msg = "For search by date start date and end date are required";

            return;
        }
        if (isNullOrEmpty(searchitem.enddate) && !isNullOrEmpty(searchitem.startdate)) {

            $scope.edrequier = true;
            $scope.msg = "For search by date start date and end date are required";
            return;
        }
        if (!isNullOrEmpty(searchitem.enddate) && !isNullOrEmpty(searchitem.startdate)) {
            if (!validation.testDate(searchitem.enddate) || !validation.testDate(searchitem.startdate)) {
                $scope.msg = "Please enter valid date";
                return

            }

            if (!validation.isStGreaterEd(searchitem.startdate, searchitem.enddate)) {
                searchitem.startdate = "";
                searchitem.enddate = "";
                $scope.msg = "End date should be greater than start date";
                return;
            }

        }


        $scope.list="";
        isFilter = true;
        getSearchData('filter', 'id', searchitem, skip, limit, function (data) {
            $scope.list = data;

            if (isNullOrEmpty(data)) {
                //isFilter = true ;
                $scope.list = [];
                $scope.noClip = true;
            }
            $("#scrollingDiv").scrollTop(0);
        })
    }

    $scope.secondIndex = function(arr){

        if($scope.sortKey == 'city'){

            return arr[0].city
        }
        if($scope.sortKey == 'postedOn'){

            return arr[0].postedOn
        }
        if($scope.sortKey == 'event_type'){

            return arr[0].event_type
        }

    }
    $('.navWrap ul').click(function () {
        $('.navWrap h3').toggleClass('open');
        $('.navWrap ul').toggleClass('open');
    });
    $(".navWrap ul").on("tap", function () {

        $('.navWrap h3').toggleClass('open');
        $('.navWrap ul').toggleClass('open');
    });
    $scope.sendSeoUrlParam = function (seourl) {
        if (seourl) {
            window.open(App_Url + 'clips' + seourl)
        }

    }

})

