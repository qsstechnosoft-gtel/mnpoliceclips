// Copyright IBM Corp. 2015. All Rights Reserved.
// Node module: loopback-example-angular
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

var loopback = require('loopback');
var bodyParser = require('body-parser');
var boot = require('loopback-boot');
var multer = require('multer');
var path=require('path');
var bodyParser = require('body-parser').urlencoded({
    extended: true
})

module.exports = { baseUrl: "http://34.192.68.83:90/" };

var app = module.exports = loopback();

sendMail=require('../server/boot/sendMail.js');
app.use(bodyParser)
app.post('/sendEmail',function(req,res,next) {
    console.log(">>>>>>>>>>>>.in send vvvvvvvvvvvvvvvvv",req.body);
    sendMail.mailToUser(req,res,function(){
        res.status(200).send('mail Send Succesfully')
    })
})
app.start = function() {
    // start the web server
    return app.listen(function() {
        app.emit('started');
        var baseUrl = app.get('url').replace(/\/$/, '');
        console.log('Web server listening at: %s', baseUrl);
        if (app.get('loopback-component-explorer')) {
            var explorerPath = app.get('loopback-component-explorer').mountPath;
            console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
        }
        var storage = multer.diskStorage({ //multers disk storage settings
            destination: function (req, file, cb) {
                cb(null, __dirname.replace("/server", "")+'/client/uploads/');
            },
            filename: function (req, file, cb) {
               // cb(null, file.originalname);
                console.log(__dirname.replace("/server", ""));
                var datetimestamp = Date.now();
                cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length - 1]);
            }
        });

        var upload = multer({ //multer settings
            storage: storage
        }).single('file');

        /** API path that will upload the files */
        app.post('/upload', function (req, res) {
            upload(req, res, function (err) {
                console.log("er>>>>>>",err)
                if (err) {
                    res.json({error_code: 1, err_desc: err});
                    console.log("err code 1", err)
                    return;
                }
                res.json({error_code: 0, err_desc: null, fileName : req.file.filename});
                console.log("err code 0")
            });
        });

    });
};
var ignoredPaths = [ '/css', '/js', '/views' ,'/upload'];

function startsWith(string, array) {

    for(i = 0; i < array.length; i++)
        if(string.startsWith(array[i]))
            return true;
    return false;
}

app.use(loopback.static(path.resolve(__dirname,'../client')));

//app.all('/*', function(req, res, next) {
//  //Redirecting to index only the requests that do not start with ignored paths
//  if(!startsWith(req.url, ignoredPaths))
//    res.sendFile('/index.html', { root: path.resolve(__dirname, '..', 'client') });
//  else
//    next();
//});
boot(app, __dirname, function(err) {
    if (err) throw err;

    // start the server if `$ node server.js`
    if (require.main === module)
        app.start();
});

