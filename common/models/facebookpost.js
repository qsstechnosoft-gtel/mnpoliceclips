'use strict';

module.exports = function (Facebookpost) {

    Facebookpost.postEvent = function (req, res, options, cb) {
        var fs = require('fs'); //FileSystem module of Node.js
         var https = require('https');
      var http = require('http');
    var FormData = require('form-data'); //Pretty multipart form maker.
    
    var ACCESS_TOKEN = "EAACEdEose0cBAF2UZBXSGcBzZATOyTp3W0ZBeLxC1INQdtVHAnxMeWLxR0TrfDeySaHV1BLQBjgRtuZBHeaItZANetkMZC4g169fHSgPOOpuAWpvvIBh1YYwwBcZBww1XkqJJdrZAyAFMMJwFSOsfZBfTJ5dihNOSmxUu7BPi9NA1mHcjftujRNiw";
  var body = '';
    var path = require('path');
    var appDir = path.dirname(require.main.filename);
    var form = new FormData(); 
    //form.append('file', fs.createReadStream('/mnt/data/mnpoliceclips/client/uploads/file-1483363293817.jpg')); //Put file
    //form.append('file', fs.createReadStream(appDir + '/../client/uploads/file-1483363293817.jpg')); //Put file
    form.append('message', "In its second week, Hello stayed at number one on the US Billboard Hot 100, selling another 635,000 digital copies marking the third-best digital sales week and the highest for a non-debut week. Hello also held atop Streaming Songs with 47.4 million US streams, down 23 percent from 61.6 million in its first week, the track also stayed atop the On-Demand Songs with 18.1 million streams. On the Radio Songs chart, Hello moved from 9 to 6, up by 46% to 106 million all-format audience impressions, thus becoming the top Airplay Gainer on the Hot 100. The track also moved from two to one on the Adult Alternative Songs airplay chart and moved nine to four on the Adult Contemporary format.[36] The following week, the song stayed at the top of the Hot 100 and Digital Songs chart, selling 480,000 downloads and becoming just the third song to sell over 400,000 copies for three straight weeks. Hello also rose from 6 to 1 on the Radio Songs chart in just its fourth week (the greatest leap to number one on the charts 25-year history), marking the quickest climb to number one on the chart in 22 years, since Mariah Careys Dreamlover reached the top in its fourth frame on 28 August 1993. Additionally, Hello became just the third song to top the Hot 100, Digital Songs, Streaming Songs, On-Demand Songs and Radio Songs tallies simultaneously in the nearly three years all five charts had coexisted. Hello remained atop the Hot 100 for ten consecutive weeks, becoming only the 31st No. 1 in the Hot 100s history to reign for at least 10 weeks, and only the 3rd for a number one debut, following One Sweet Day by Mariah Carey and Boyz II Men (16 weeks) and Candle in the Wind 1997/Something About the Way You Look Tonight by Elton John (14 weeks). By spending a tenth week at the top of the chart, it became Adeles longest-running number-one single and the longest-leading Hot 100 No. 1 by a solo female since Rihannas We Found Love, featuring Calvin Harris, which also led for 10 weeks in 2011–2012. As of January 2016, it had sold 3.7 million downloads.[37] The Recording Industry Association of America certified the song quadruple platinum.[38] The single also benefitted from numerous Dance/EDM remixes as well,[39] thus resulting in Hello topping Billboards Dance Club Songs and Dance/Mix Show Airplay charts.[40][41] On the chart dated 23 April 2016, the song spent a 21st week at the top of the Adult Contemporary Chart, matching the record set by Kelly Clarksons Breakaway (2005) and Celine Dions A New Day Has Come (2002) for the longest No. 1 run among women since the list launched in 1961. It also equaled the third-longest stay at the summit among all acts.[42]Hello entered at the top of the Canadian Hot 100 as the 100th song to top the chart, selling 140,000 copies and outsold Justin Biebers Sorry, which sold 40,000 units and debuted at number two the same week. The song was streamed 4.79 million times, setting a record for the most streamed track in a week.[43]"); //Put message
      form.append('link', 'https://i.imgur.com/nCmZetH.png');
    
    //POST request options, notice 'path' has access_token parameter
    var options = {
        method: 'post',
        host: 'graph.facebook.com',
        path: '/me/feed?access_token='+ACCESS_TOKEN,
        headers: form.getHeaders(),
}
  var util = require('util');
//Do POST request, callback for response
var request = https.request(options, function (res1){
     console.log(res1);
  	 res.send(util.inspect(res1));
});
 
//Binds form to request
form.pipe(request);
 
//If anything goes wrong (request-wise not FB)
request.on('error', function (error) {
     console.log(error);
  	error.e = 'Error received';
  	 res.send(error);
});
 request.end();
    }
    Facebookpost.remoteMethod('postEvent', {
        description: 'evnt post on facebook',
        accepts: [
            { arg: 'req', type: 'string', 'http': { source: 'req' } },
            { arg: 'res', type: 'object', 'http': { source: 'res' } },
            { arg: 'options', type: 'string', required: true, http: { source: 'body' } }
        ],
        http: { verb: 'get', path: '/postEvent' },
        returns: {
            arg: 'object', type: 'object', root: true
        }
    });
};
function saveError(endpoint, data, success) {
    var http = require('http');
    var dataString = JSON.stringify(data);
    //var dataString = data;
    console.log(':::::::::::::', dataString)
    var headers = {
        'Content-Type': 'application/json'
    };
    var options = {
        host: '34.192.68.83',
        path: endpoint,
        method: 'POST',
        family: 4,
        port: 80,
        headers: headers
    };
    console.log('option>>..', options)
    var req = http.request(options, function (res) {
        var responseString = '';

        res.on('data', function (data) {
            console.log('<<<<<<<<<<error<<<<<<<<<<<<');
            responseString += data;
        });

        res.on('end', function () {
            console.log(responseString);
            console.log('error response>>>>>>>>>>>>>>>>>>>>>>>>..');
            var responseObject = JSON.parse(responseString);
            success(responseObject);
        });
    });
    req.write(dataString);
    req.end();
}