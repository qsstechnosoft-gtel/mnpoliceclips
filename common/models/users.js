var sendMail=require('../../server/boot/sendMail');
var bcrypt = require('bcrypt');
module.exports = function(UserRegistration) {
  UserRegistration.forgotPassword=function(req,res,options, cb){
    // var MyUser= UserRegistration.app.models.User;
    cb = cb || utils.createPromiseCallback();
    options = options || {};
    if (typeof options.email !== 'string') {
      var err = new Error('Email is required');
      err.statusCode = 400;
      err.code = 'EMAIL_REQUIRED';
      cb(err);
      return cb.promise;
    }
    UserRegistration.findOne({ where:{email: options.email} }, function(err,user) {
      console.log(">>>>>>>>>>>>>>>>>.user is>>>>",user,err)
      if (err) {
        return cb(err);
      }
      if (!user) {
        err = new Error('Email not found');
        err.statusCode = 404;
        err.code = 'EMAIL_NOT_FOUND';
        return cb(err);
      }
      function generateOtp()
      {
        var text = "";
        var possible = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        for( var i=0; i < 6; i++ )
          text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
      }
      var otp=generateOtp();
      //var hashPassword=MyUser.hashPassword(options.password);
     
        user.updateAttribute('password',otp,function(err){
          user['otp']=otp;
          if (err) return cb(null,false);
          console.log(">>>>>>>>>>>>err in updating coach",err);
          if (err) return cb(null,false);
          //logger.debug("successfully updated the otp in user registration");
          console.log(">>>>>>>>>>>>.after update>>>");
          var mailOptions={
            fromEmail:'pmalikbtech@gmail.com',
            toEmail:[user.email],
            name:user.firstname+' '+user.lastname,
            message:'Forgot Password',
            templateData:user,
            template:'forgorpassword_email'
          };
          req['data']=mailOptions;

          sendMail.mailToUser(req,res,function(){
            user.accessTokens.create({
              ttl: 5
            }, function(err, accessToken) {
              if (err) {
                return cb(err);
              }

              res.send(accessToken);
            })

          })


        })
      



    });
    //return cb.promise;
  }
  UserRegistration.verifyOtp=function(req,res,options, cb){
    cb = cb || utils.createPromiseCallback();
    options = options || {};
    if (typeof options.email !== 'string') {
      var err = new Error('Email is required');
      err.statusCode = 400;
      err.code = 'EMAIL_REQUIRED';
      cb(err);
      return cb.promise;
    }
    if (typeof options.otp !== 'string') {
      var err = new Error('Otp is required');
      err.statusCode = 400;
      err.code = 'OTP_REQUIRED';
      cb(err);
      return cb.promise;
    }
    UserRegistration.findOne({ where:{email: options.email}}, function(err,user) {
      if (err) {
        return cb(err);
      }
      if (!user) {
        err = new Error('Email not found');
        err.statusCode = 404;
        err.code = 'EMAIL_NOT_FOUND';
        return cb(err);
      }
      console.log(">>>>>>>>>>>>>>>>>>>>..user otp is>>>",user.otp,">>>nnnn>>>",options.otp,user.otp==options.otp)
      if(user.otp==options.otp){
        var sendData={
          status:200,
          otp:true
        }
        res.send(sendData);
      }
      else{
        err = new Error('Invalid Otp');
        err.statusCode = 404;
        err.code = 'INVALID OTP';
        return cb(err);
      }

    });
    //return cb.promise;
  }
  UserRegistration.changePassword=function(req,res,options, cb){
    var MyUser= UserRegistration.app.models.User;
    cb = cb || utils.createPromiseCallback();
    options = options || {};
    if (typeof options.email !== 'string') {
      var err = new Error('Email is required');
      err.statusCode = 400;
      err.code = 'EMAIL_REQUIRED';
      cb(err);
      return cb.promise;
    }
    if (!options.password) {
      var err = new Error('password is required');
      err.statusCode = 400;
      err.code = 'PASSWORD_REQUIRED';
      cb(err);
      return cb.promise;
    }
    UserRegistration.findOne({ where:{email: options.email}}, function(err,user) {
      var hashPassword=MyUser.hashPassword(options.password);
      if(hashPassword){
        user.updateAttribute('password',hashPassword,function(err){
          if (err) return cb(null,false);
          var data123={
            "email":"pmalikbtech@gmail.com",
            "password":hashPassword
          };
          user.accessTokens.create({
            ttl: 5
          }, function(err, accessToken) {
            if (err) {
              return cb(err);
            }

            res.send(accessToken);
          })

        })
      }
    })


  }
  UserRegistration.passwordChange=function(req,res,options, cb){
    var MyUser= UserRegistration.app.models.User;
    console.log(">>>>>>>>>>>>>>>>>>.req ggggggggggg is>>>>>",options.id);
    //cb = cb || utils.createPromiseCallback();
    options = options || {};
    /* if (typeof options.email !== 'string') {
     var err = new Error('Email is required');
     err.statusCode = 400;
     err.code = 'EMAIL_REQUIRED';
     cb(err);
     return cb.promise;
     }*/
    UserRegistration.findOne({ where:{id: options.id} }, function(err,user) {
      console.log(">>>>>>>>>>>>>>>>>password>>>",options,">>>>>>>>>user pass is>>",user)
      MyUser.hasPassword1(options.oldpassword,user.password,function(value,status){
        console.log(">>>>>>>>>>>>>>.status is>>>",status)
        if(status){
          var hashPassword=MyUser.hashPassword(options.password);
          user.updateAttribute('password',hashPassword,function(err){
            if (err) return cb(null,false);
            res.status(200).send('password changed Successfully');

          })
        }
        else{
          res.status(401).send('password unmatched');
        }
      });

    });
    //return cb.promise;
  }
  UserRegistration.remoteMethod ('passwordChange', {
    description: 'change password by USer',
    accepts: [
      {arg: 'req', type: 'object', 'http': {source: 'req'}},
      {arg: 'res', type: 'object', 'http':{source: 'res'}},
      {arg: 'options', type: 'object', required: true, http: {source: 'body'}}
    ],
    http: {verb: 'post', path: '/password-change'},
    returns: {
      arg: 'object', type: 'object', root: true
    }
  });
  UserRegistration.remoteMethod ('changePassword', {
    description: 'Change Password when forget',
    accepts: [
      {arg: 'req', type: 'object', 'http': {source: 'req'}},
      {arg: 'res', type: 'object', 'http':{source: 'res'}},
      {arg: 'options', type: 'object', required: true, http: {source: 'body'}}
    ],
    http: {verb: 'post', path: '/changePassword'},
    returns: {
      arg: 'object', type: 'object', root: true
    }
  });
  UserRegistration.remoteMethod ('verifyOtp', {
    description: 'verfiy otp to change password',
    accepts: [
      {arg: 'req', type: 'object', 'http': {source: 'req'}},
      {arg: 'res', type: 'object', 'http':{source: 'res'}},
      {arg: 'options', type: 'object', required: true, http: {source: 'body'}}
    ],
    http: {verb: 'post', path: '/verifyotp'},
    returns: {
      arg: 'object', type: 'object', root: true
    }
  });
  UserRegistration.remoteMethod ('forgotPassword', {
    description: 'change password for a user with email when forgot.',
    accepts: [
      {arg: 'req', type: 'object', 'http': {source: 'req'}},
      {arg: 'res', type: 'object', 'http':{source: 'res'}},
      {arg: 'options', type: 'object', required: true, http: {source: 'body'}}
    ],
    http: {verb: 'post', path: '/forgot-password'},
    returns: {
      arg: 'object', type: 'object', root: true
    }
  });
}
